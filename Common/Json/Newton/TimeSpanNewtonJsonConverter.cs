﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;


namespace Common.Json.Newton
{
    public class TimeSpanNewtonJsonConverter : JsonConverter<TimeSpan>
    {
        public override TimeSpan ReadJson(JsonReader reader, Type objectType, TimeSpan existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.Value==null)
            {
                //? default(DateTime?) : DateTime.Parse(reader.GetString());]
                return default(TimeSpan);
            }
            //if (TimeSpan.TryParse(reader.GetString(), out TimeSpan date))
            //    return date;
            TimeSpan.TryParse(reader.ReadAsString(), out TimeSpan date);
            return date;
        }

        public override void WriteJson(JsonWriter writer, TimeSpan value, JsonSerializer serializer)
        {
            // writer.WriteStringValue(value.ToString("HH:mm:ss"));
            writer.WriteValue(value.ToString());
        }
    }
}
