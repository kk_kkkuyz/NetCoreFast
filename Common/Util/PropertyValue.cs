﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace Common.Util
{
    /// <summary>
    /// 高性能动态获取对象属性值.有bug  对于值类型。
    /// <see cref="https://blog.csdn.net/chinaherolts2008/article/details/115560854"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PropertyValue<T>
    {
        private static ConcurrentDictionary<string, MemberGetDelegate> _memberGetDelegate = new ConcurrentDictionary<string, MemberGetDelegate>();
        delegate object MemberGetDelegate(T obj);
        public T Target { get; private set; }
        public PropertyValue(T obj)
        {
            Target = obj;
        }
        public object Get(PropertyInfo p)
        {
            MemberGetDelegate memberGet = _memberGetDelegate.GetOrAdd(p.Name, BuildDelegate);
            if (memberGet == null) return p.GetValue(Target);
            return memberGet(Target);
        }
        private MemberGetDelegate BuildDelegate(string name)
        {
            Type type = typeof(T);
            PropertyInfo property = type.GetProperty(name);
            MethodInfo methodInfo = GetMethodInfo("_" + name, type);// property.GetGetMethod();
            Type propertyType = property.PropertyType;
            Console.WriteLine(methodInfo);
            //We need to check whether the property is NULLABLE
            if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                //Console.WriteLine(property.GetGetMethod());
                //If it is NULLABLE, then get the underlying type. eg if "Nullable<int>" then this will return just "int"
                propertyType = propertyType.GetGenericArguments()[0];
                methodInfo = propertyType.GetMethod("get_" + propertyType.Name);
                Console.WriteLine(propertyType + "-" + methodInfo);
                //Console.WriteLine(property.GetGetMethod());
                //Console.WriteLine(property.GetGetMethod().IsGenericMethod);
                //Console.WriteLine(property.GetGetMethod().GetBaseDefinition());
            }
            return (MemberGetDelegate)Delegate.CreateDelegate(typeof(MemberGetDelegate), Target, "get_" + name);
            // return (MemberGetDelegate)Delegate.CreateDelegate(typeof(MemberGetDelegate), methodInfo);
        }
        private MethodInfo GetMethodInfo(string methodName, Type type)
        {
            var methodInfo = type.GetMethod(methodName, BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);
            return methodInfo;
        }
    }
}
