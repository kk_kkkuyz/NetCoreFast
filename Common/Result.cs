﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common
{
    /// <summary>
    /// 结果数据
    /// </summary>
    public class Result<T>
    {
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 状态码
        /// </summary>
        public int  Code { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 响应数据
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// 实例化对象
        /// </summary>
        /// <param name="status"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static Result<T> Instance(bool status, string msg)
        {
            return new Result<T>() { Status = status,Code=500, Msg = msg };
        }
        /// <summary>
        /// 实例化对象
        /// </summary>
        /// <param name="status"></param>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static Result<T> Instance(bool status,int  code, string msg)
        {
            return new Result<T>() { Status = status, Code = code, Msg = msg };
        }
        /// <summary>
        /// 实例化错误对象
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static Result<T> Error(string msg)
        {
            return new Result<T>() { Status = false, Code = 500, Msg = msg };
        }
        /// <summary>
        /// 实例化成功对象
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static Result<T> Success(string msg= "succeed")
        {
            return new Result<T>() { Status = true, Code = 200, Msg = msg };
        }
        /// <summary>
        /// 设置返回数据
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public  Result<T> SetData(T obj)
        {
            this.Data = obj;
            return this;
        }
        /// <summary>
        /// 设置返回码
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        public Result<T> SetCode(int Code)
        {
            this.Code = Code;
            return this;
        }
    }
}