﻿/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.MyAttribute
{
    /// <summary>
    /// 指明那些类不需要被Autofac注入的注解
    /// </summary>
    public class NotAutofacAttribute : Attribute
    {
        /// <summary>
        /// 判断是否不自动注入，如果加了NotAutofacAttribute注解则不自动注入
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNotAutofac(Type type)
        {
            var objs = type.GetCustomAttributes(typeof(NotAutofacAttribute), false);//获取字段          
            return objs.Length > 0;
        }
    }
}
