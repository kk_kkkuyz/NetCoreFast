﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace Common.MyAttribute
{
    /// <summary>
    /// 用于标注字段值查询来自哪个表的的哪一列(仅限于有关联的表中)，示例：实体类名1（表1）.实体类名2（表2）.实体类名3（表3）....字段名（列名）
    /// <see cref="https://www.cnblogs.com/castyuan/p/10186619.html"/> 
    /// </summary>
    public class FromModelAttribute : Attribute
    {
        /// <summary>
        /// 类名(表名)
        /// </summary>
        public string[] ModelNames { get; }

        /// <summary>
        /// 字段(列名)
        /// </summary>
        public string ModelColumn { get; }
        /// <summary>
        /// 查询字段所在导航属性路径
        /// </summary>
        public string ModelColumnPath { get; }
        /// <summary>
        /// 查询字段所在导航属性路径。示例：实体类名1（表1）.实体类名2（表2）.实体类名3（表3）....字段名（列名）
        /// </summary>
        public FromModelAttribute(string entityName)
        {
            if (string.IsNullOrWhiteSpace(entityName))
            {
                throw new ArgumentException();
            }
            ModelColumnPath = entityName;
            var names = entityName.Split('.');
            ModelColumn = names[names.Length - 1];
            if (names.Length > 1)
            {
                ModelNames = new string[names.Length - 1];
                for (int i = 0; i < names.Length - 1; i++)
                {
                    ModelNames[i] = names[i];
                }
            }
        }
        /// <summary>
        /// 查询字段所在导航属性路径。列名 + 该列所在的表名 + 该列所在的表的上一级表名0+……。示例：字段名（列名），实体类名1（表1），实体类名2（表2），实体类名3（表3）....
        /// </summary>
        /// <param name="entityColuum"></param>
        /// <param name="entityNames"></param>
        public FromModelAttribute(string entityColuum, params string[] entityNames)
        {
            ModelNames = entityNames;
            ModelColumn = entityColuum;
            ModelColumnPath = string.Join(".", entityNames) + "." + entityColuum;
        }
    }
}
