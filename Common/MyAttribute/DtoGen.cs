﻿/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.MyAttribute
{
    /// <summary>
    /// dto生成行为注解
    /// <remark>
    /// 指明实体属性对于生成list、add、update、detail哪种行为不需要输出或显示到前台界面
    /// </remark>
    /// </summary>
    public class DtoGenAttribute : Attribute
    {
        /// <summary>
        /// 列表查询dto不输出此属性
        /// </summary>
        public bool NotList
        {
            get;
            set;     
        }=false;
        /// <summary>
        /// 添加模型dto不输出此属性
        /// </summary>
        public bool NotAdd
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 修改模型dto不输出此属性
        /// </summary>
        public bool NotUpdate
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 添加修改模型dto不输出此属性
        /// </summary>
        public bool NotAddUpdate
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 详细查询模型dto不输出此属性
        /// </summary>
        public bool NotDetail
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 标志此属性生成到添加修改dto，但是不显示到UI界面，主要用于系统自动赋值
        /// </summary>
        public bool AddUpdateButInvisible
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 标志此属性生成到添加dto，但是不显示到UI界面，主要用于系统自动赋值
        /// </summary>
        public bool AddButInvisible
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 标志此属性生成到修改dto，但是不显示到UI界面，主要用于系统自动赋值
        /// </summary>
        public bool UpdateButInvisible
        {
            get;
            set;
        } = false;
    }
}
