﻿using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading.Tasks;

namespace Plugin
{
    /// <summary>  
    /// 插件帮助类  
    /// </summary>  
    public static class PluginHelper
    {
        /// <summary>  
        /// 获取插件加载的信息  
        /// </summary>  
        /// <param name="rootPath"></param>  
        /// <returns></returns>  
        /// <exception cref="ArgumentException"></exception>  
        public static List<IPlugin> GetPlugins(string rootPath)
        {
            var plugins = new List<IPlugin>();
            Directory.CreateDirectory(Path.Combine(rootPath, "Plugins"));
            var pluginsDir = Path.Combine(rootPath, "Plugins");
            foreach (var dir in Directory.GetDirectories(pluginsDir))
            {
                var dirName = Path.GetFileName(dir);
                var pluginName = dirName + ".dll";
                var pluginDll = Path.Combine(dir, pluginName);
                if (!File.Exists(pluginDll))
                {
                    continue;
                }
                var loader = new AssemblyLoader(dir);
                var types = loader.Load(pluginName);
                var instances = types.Where(t => typeof(IPlugin).IsAssignableFrom(t) && !t.IsAbstract)
                    .Select(x => Activator.CreateInstance(x) as IPlugin);
                foreach (var i in instances)
                {
                    if (string.IsNullOrEmpty(i.Info().Name)) i.Info().Name = pluginName;
                    if (string.IsNullOrEmpty(i.Info().DisplayName)) i.Info().DisplayName = pluginName;
                    i.Info().Path = dir;
                    var assembly = i.GetType().Assembly;
                    i.Info().AssemblyName = assembly.GetName().Name;
                    i.Info().Version = assembly.GetName().Version?.ToString();
                    i.Info().Assembly = assembly;                   
                    plugins.Add(i);
                }
            }
            return plugins;
        }
    }
}
