﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading.Tasks;

namespace Plugin
{
    /// <summary>
    /// 程序集加载器
    /// <seealso cref="http://t.zoukankan.com/Leo_wl-p-15708371.html"/>
    /// <seealso cref="https://www.1024sky.cn/blog/article/53738"/>
    /// </summary>
    public class AssemblyLoader
    {
        private string _basePath;
        private AssemblyLoadContext context;

        public AssemblyLoader(string basePath)
        {
            _basePath = basePath;
        }
        /// <summary>  
        /// 加载程序集  
        /// </summary>  
        /// <param name="dllFileName"></param>  
        /// <returns></returns>  
        public Assembly LoadAssembly(string dllFileName)
        {
            context = new AssemblyLoadContext(dllFileName);
            context.Resolving += Context_Resolving;
            //需要绝对路径
            string path = Path.Combine(_basePath, dllFileName);
            if (File.Exists(path))
            {
                using (var stream = File.OpenRead(path))
                {
                    Assembly assembly = context.LoadFromStream(stream);
                    return assembly;
                }
            }
            else
            {
                Console.WriteLine($"加载动态dll失败:{dllFileName}不存在：{path}");
            }
            return null;
        }
        public Type[] Load(string dllFileName)
        {
            var assembly = LoadAssembly(dllFileName);
            if (assembly != null) return assembly.GetTypes();
            return new Type[] { };
        }
        public Type Load(string dllFileName, string typeName)
        {
            return Load(dllFileName).Where(o => o.Name == typeName).FirstOrDefault();
        }

        /// <summary>
        /// 加载依赖文件
        /// </summary>
        /// <param name="context"></param>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        private Assembly Context_Resolving(AssemblyLoadContext context, AssemblyName assemblyName)
        {
            string expectedPath = Path.Combine(_basePath, assemblyName.Name + ".dll"); ;
            if (File.Exists(expectedPath))
            {
                try
                {
                    using (var stream = File.OpenRead(expectedPath))
                    {
                        return context.LoadFromStream(stream);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"加载节点{expectedPath}发生异常：{ex.Message},{ex.StackTrace}");
                }
            }
            else
            {
                Console.WriteLine($"依赖文件不存在：{expectedPath}");
            }
            return null;
        }
    }
}
