﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugin
{
    /// <summary>
    /// https://www.dongchuanmin.com/net/4240.html
    /// </summary>
    public interface IPlugin
    {
        //void Register(CancellationToken cancellationToken, params object[] args);
        PluginInfo Info()=> new PluginInfo();
        void Init(WebApplication webApplication);
        /// <summary>
        /// 执行插件方法
        /// </summary>
        /// <param name="args">参数</param>
        /// <returns></returns>
        object Execute(params object[] args);
        void Stop(CancellationToken cancellationToken);
    }
}
