using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Model.Entity.Sys;
using Model;

namespace Web.DB
{
    /// <summary>
    /// 初始化数据   
    /// </summary>
    /// <remarks>本类初始化数据为代码生成器自动生成，自定义初始化数据请在InitDatabase.cs添加</remarks>
    public static class InitDatas
    {
        public static void Init(MyDbContext context)
        {
            var ress = new SysRes[]
            {
            
               new SysRes {Name="地区列表",Value="Sys_SysArea_List"},
               new SysRes {Name="地区详情",Value="Sys_SysArea_Get"},
               new SysRes {Name="地区添加",Value="Sys_SysArea_Add"},
               new SysRes {Name="地区修改",Value="Sys_SysArea_Update"},
               new SysRes {Name="地区删除",Value="Sys_SysArea_Delete"},
               new SysRes {Name="地区批量删除",Value="Sys_SysArea_BatchDelete"},
            
               new SysRes {Name="权限列表",Value="Sys_SysAuthority_List"},
               new SysRes {Name="权限详情",Value="Sys_SysAuthority_Get"},
               new SysRes {Name="权限添加",Value="Sys_SysAuthority_Add"},
               new SysRes {Name="权限修改",Value="Sys_SysAuthority_Update"},
               new SysRes {Name="权限删除",Value="Sys_SysAuthority_Delete"},
               new SysRes {Name="权限批量删除",Value="Sys_SysAuthority_BatchDelete"},
            
               new SysRes {Name="权限资源列表",Value="Sys_SysAuthorityRes_List"},
               new SysRes {Name="权限资源详情",Value="Sys_SysAuthorityRes_Get"},
               new SysRes {Name="权限资源添加",Value="Sys_SysAuthorityRes_Add"},
               new SysRes {Name="权限资源修改",Value="Sys_SysAuthorityRes_Update"},
               new SysRes {Name="权限资源删除",Value="Sys_SysAuthorityRes_Delete"},
               new SysRes {Name="权限资源批量删除",Value="Sys_SysAuthorityRes_BatchDelete"},
            
               new SysRes {Name="系统配置列表",Value="Sys_Sysconfig_List"},
               new SysRes {Name="系统配置详情",Value="Sys_Sysconfig_Get"},
               new SysRes {Name="系统配置添加",Value="Sys_Sysconfig_Add"},
               new SysRes {Name="系统配置修改",Value="Sys_Sysconfig_Update"},
               new SysRes {Name="系统配置删除",Value="Sys_Sysconfig_Delete"},
               new SysRes {Name="系统配置批量删除",Value="Sys_Sysconfig_BatchDelete"},
            
               new SysRes {Name="部门列表",Value="Sys_SysDepartment_List"},
               new SysRes {Name="部门详情",Value="Sys_SysDepartment_Get"},
               new SysRes {Name="部门添加",Value="Sys_SysDepartment_Add"},
               new SysRes {Name="部门修改",Value="Sys_SysDepartment_Update"},
               new SysRes {Name="部门删除",Value="Sys_SysDepartment_Delete"},
               new SysRes {Name="部门批量删除",Value="Sys_SysDepartment_BatchDelete"},
            
               new SysRes {Name="文档列表",Value="Sys_SysDocument_List"},
               new SysRes {Name="文档详情",Value="Sys_SysDocument_Get"},
               new SysRes {Name="文档添加",Value="Sys_SysDocument_Add"},
               new SysRes {Name="文档修改",Value="Sys_SysDocument_Update"},
               new SysRes {Name="文档删除",Value="Sys_SysDocument_Delete"},
               new SysRes {Name="文档批量删除",Value="Sys_SysDocument_BatchDelete"},
            
               new SysRes {Name="资源列表",Value="Sys_SysRes_List"},
               new SysRes {Name="资源详情",Value="Sys_SysRes_Get"},
               new SysRes {Name="资源添加",Value="Sys_SysRes_Add"},
               new SysRes {Name="资源修改",Value="Sys_SysRes_Update"},
               new SysRes {Name="资源删除",Value="Sys_SysRes_Delete"},
               new SysRes {Name="资源批量删除",Value="Sys_SysRes_BatchDelete"},
            
               new SysRes {Name="角色列表",Value="Sys_SysRole_List"},
               new SysRes {Name="角色详情",Value="Sys_SysRole_Get"},
               new SysRes {Name="角色添加",Value="Sys_SysRole_Add"},
               new SysRes {Name="角色修改",Value="Sys_SysRole_Update"},
               new SysRes {Name="角色删除",Value="Sys_SysRole_Delete"},
               new SysRes {Name="角色批量删除",Value="Sys_SysRole_BatchDelete"},
            
               new SysRes {Name="角色权限列表",Value="Sys_SysRoleAuthority_List"},
               new SysRes {Name="角色权限详情",Value="Sys_SysRoleAuthority_Get"},
               new SysRes {Name="角色权限添加",Value="Sys_SysRoleAuthority_Add"},
               new SysRes {Name="角色权限修改",Value="Sys_SysRoleAuthority_Update"},
               new SysRes {Name="角色权限删除",Value="Sys_SysRoleAuthority_Delete"},
               new SysRes {Name="角色权限批量删除",Value="Sys_SysRoleAuthority_BatchDelete"},
            
               new SysRes {Name="用户列表",Value="Sys_SysUser_List"},
               new SysRes {Name="用户详情",Value="Sys_SysUser_Get"},
               new SysRes {Name="用户添加",Value="Sys_SysUser_Add"},
               new SysRes {Name="用户修改",Value="Sys_SysUser_Update"},
               new SysRes {Name="用户删除",Value="Sys_SysUser_Delete"},
               new SysRes {Name="用户批量删除",Value="Sys_SysUser_BatchDelete"},
            
               new SysRes {Name="访问记录列表",Value="Sys_SysVisitRecord_List"},
               new SysRes {Name="访问记录详情",Value="Sys_SysVisitRecord_Get"},
               new SysRes {Name="访问记录添加",Value="Sys_SysVisitRecord_Add"},
               new SysRes {Name="访问记录修改",Value="Sys_SysVisitRecord_Update"},
               new SysRes {Name="访问记录删除",Value="Sys_SysVisitRecord_Delete"},
               new SysRes {Name="访问记录批量删除",Value="Sys_SysVisitRecord_BatchDelete"},
            
            
            };
            context.SysRess.AddRange(ress);
        }
    }
}
