﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Extension
{
    /// <summary>
    /// 分布式缓存扩展方法
    /// </summary>
    public static class DistributedCacheExtensions
    {
        /// <summary>
        /// 判断键是否存在
        /// </summary>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Exists(this IDistributedCache cache, string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            return cache.Get(key) != null;
        }
        /// <summary>
        /// 根据键获取值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(this IDistributedCache cache, string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            var buff = cache.Get(key);
            return Buff2Objectt<T>(buff);
        }
        public static async Task<T> GetAsync<T>(this IDistributedCache cache, string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            var buff = await cache.GetAsync(key);
            return Buff2Objectt<T>(buff);
        }
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public static void Set<T>(this IDistributedCache cache, string key, T val)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            byte[] serializedResult = Object2ByteArray<T>(val);
            cache.Set(key, serializedResult,new DistributedCacheEntryOptions() { SlidingExpiration=TimeSpan.MaxValue});
        }
        public static async Task SetAsync<T>(this IDistributedCache cache, string key, T val)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            byte[] serializedResult = Object2ByteArray<T>(val);
            await cache.SetAsync(key, serializedResult, new DistributedCacheEntryOptions() { SlidingExpiration = TimeSpan.MaxValue });
        }
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="timeSpan">过期时间，相对当前时间</param>
        public static void Set<T>(this IDistributedCache cache, string key, T val, TimeSpan timeSpan)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            byte[] serializedResult = Object2ByteArray<T>(val);
            cache.Set(key, serializedResult, new DistributedCacheEntryOptions() { AbsoluteExpirationRelativeToNow = timeSpan });
        }
        public static async Task SetAsync<T>(this IDistributedCache cache, string key, T val, TimeSpan timeSpan)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            byte[] serializedResult = Object2ByteArray<T>(val);
            await cache.SetAsync(key, serializedResult, new DistributedCacheEntryOptions() { AbsoluteExpirationRelativeToNow = timeSpan });
        }
        /// <summary>
        /// 获取或设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="setDataCallback">设置数据</param>
        /// <param name="exp">过期时间</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static T GetOrSet<T>(this IDistributedCache cache, string key, Func<T> setDataCallback, TimeSpan? exp = null)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            T data;
            if (!cache.Exists(key))
            {
                data = setDataCallback();
                if (data == null)
                {
                    return default(T);//data
                }
                if (exp.HasValue)
                {
                    cache.Set(key, data, exp.Value);
                }
                else
                {
                    cache.Set(key, data);
                }
            }
            else
            {
                data = cache.Get<T>(key);
            }
            return data;
        }
        public static async Task<T> GetOrSetAsync<T>(this IDistributedCache cache, string key, Func<Task<T>> setDataCallback, TimeSpan? exp = null)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid cache key");
            T data;
            if (!cache.Exists(key))
            {
                data = await setDataCallback();
                if (data == null)
                {
                    return default(T);//data
                }
                if (exp.HasValue)
                {
                    cache.Set<T>(key, data, exp.Value);
                }
                else
                {
                    cache.Set<T>(key, data);
                }
            }
            else
            {
                data = cache.Get<T>(key);
            }
            return data;
        }

        private static byte[] Object2ByteArray<T>(T val)
        {
            // 对象先转换为 json对象字符串
            string json = JsonConvert.SerializeObject(val);
            //json对象字符串转为byte[]
            return Encoding.UTF8.GetBytes(json);
        }
        private static string Buff2String(byte[] buff)
        {
            // byte[]先转换为json对象字符串
            return Encoding.UTF8.GetString(buff);
        }
        private static T Buff2Objectt<T>(byte[] buff)
        {
            if (buff == null) return default(T);
            // byte[]先转换为json对象字符串
            var strJson = Buff2String(buff);
            // 把json对象字符串转换为指定对象 需要using Newtonsoft.Json;            
            return JsonConvert.DeserializeObject<T>(strJson);
        }
    }
}
