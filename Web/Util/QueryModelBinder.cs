﻿using Baidu.Aip;
using Common;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using System;
using System.Text;
using System.Threading.Tasks;
using Common;
using IdentityModel;
using Model.Entity.Sys;
using Common.Util;
using NPOI.SS.Formula.Functions;
using System.Reflection;
using NPOI.SS.Formula.Eval;
using System.Collections.Generic;

namespace Web.Util
{
    /// <summary>
    /// 自定义查询参数模型绑定
    /// <see cref="https://learn.microsoft.com/zh-cn/aspnet/core/mvc/advanced/custom-model-binding?view=aspnetcore-7.0"/>
    /// </summary>
    public class QueryModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }
            //获取绑定参数变量名，如:
            //[HttpGet("test/{name}")]public Result<object> Test(string name)中方法中的参数name,与路由中的name保持一致，请求方式http://localhost:5000/api/test/222222
            //[HttpGet("test")]public Result<object> Test([FromQuery] string name)中方法中的参数name,请求方式http://localhost:5000/api/test?name=222222
            //var modelName = bindingContext.ModelName;
            //根据名称获取传递的值
            //ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
            //获取所有查询参数
            var queryStrs = bindingContext.HttpContext.Request.Query;
            var gt = bindingContext.ModelType.GetGenericArguments()[0];//获取QueryParam参数具体类型
            Type classType = typeof(QueryParam<>);
            classType = classType.MakeGenericType(gt);
            var instance = Activator.CreateInstance(classType);//创建实例对象
            Dictionary<string, string> where = new Dictionary<string, string>();//条件
            foreach (var q in queryStrs)
            {
                if (!q.Key.StartsWith("_"))//_开头的不是条件查询
                {
                    where.Add(q.Key, q.Value);
                }
            }
            classType.InvokeMember("Parse", BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Instance, null, instance, new Dictionary<string,string>[] { where });          
            bindingContext.Result = ModelBindingResult.Success(instance);
            return Task.CompletedTask;
        }
    }
    public class QueryModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (context.Metadata.ModelType.HasImplementedRawGeneric(typeof(QueryParam<>)))
            {
                return new BinderTypeModelBinder(typeof(QueryModelBinder));
            }
            //注意：上述代码返回 BinderTypeModelBinder。 BinderTypeModelBinder 充当模型绑定器中心，并提供依赖关系注入 (DI)。 需要 AuthorEntityBinder DI 才能访问 EF Core。 
            //如果模型绑定器需要 DI 中的服务，请使用 BinderTypeModelBinder。
            return null;
        }
    }
}
