﻿using Common.MyAttribute;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Util
{
    /// <summary>
    /// <see cref="https://www.cnblogs.com/artech/archive/2022/03/16/16011320.html"/>
    /// <see cref="https://www.cnblogs.com/luminqiangblogs/p/14245584.html"/>
    /// </summary>
    [NotAutofac]
    internal class LifetimeEventsHostedService : IHostedService
    {
        //private readonly string appCode = "";// ConfigurationManager.GetAppSetting("AppCode");
        //private readonly string appName = "应用";// ConfigurationManager.GetAppSetting("AppName");
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly IServiceProvider _serviceProvider;
        private IDisposable _tokenSource;
        private IEnumerable<IWebApp> _webapps;

        public LifetimeEventsHostedService(IHostApplicationLifetime appLifetime, IServiceProvider serviceProvider)
        {
            _appLifetime = appLifetime;
            _serviceProvider = serviceProvider;
            _webapps = _serviceProvider.GetServices<IWebApp>();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _appLifetime.ApplicationStarted.Register(OnStarted);
            _appLifetime.ApplicationStopping.Register(OnStopping);
            _appLifetime.ApplicationStopped.Register(OnStopped);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _tokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(1)).Token.Register(_appLifetime.StopApplication);
            _tokenSource?.Dispose();
            return Task.CompletedTask;
        }

        private void OnStarted()
        {
            //启动应用                    
            foreach (var app in _webapps)
            {
                app.OnStarted();
            }
            var interNetworkV6 = System.Net.Sockets.AddressFamily.InterNetworkV6;
            var interNetwork = System.Net.Sockets.AddressFamily.InterNetwork;
            var ipList = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
              .Select(p => p.GetIPProperties())
              .SelectMany(p => p.UnicastAddresses)
              .Where(p => (p.Address.AddressFamily == interNetwork || p.Address.AddressFamily == interNetworkV6) && !System.Net.IPAddress.IsLoopback(p.Address)).ToList();
            //Console.WriteLine($"info: 启动成功!【{DateTime.Now}】 【{ipList[1].Address}】 【{ipList[0].Address}】");
            NLogHelper.logger.Info($"启动成功!【IPV4:{ipList[1].Address}】 【IPV6:{ipList[0].Address}】");
        }

        private void OnStopping()
        {
            //Console.WriteLine($"info: 正在停止……{DateTime.Now}】");
            NLogHelper.logger.Info($"正在停止……");
        }

        private void OnStopped()
        {
            //Console.WriteLine($"info: 停止成功!【{DateTime.Now}】");
            NLogHelper.logger.Info($"停止成功!");
        }
    }
    /// <summary>
    /// 随着应用启动而启动的程序
    /// </summary>
    public interface IWebApp
    {
        /// <summary>
        /// 应用名称
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 应用描述
        /// </summary>
        public string Desc { get; }
        void OnStarted();
        void OnStopping();
        void OnStopped();
    }
}
