﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.IO.Compression;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Linq;
using System.Net.Cache;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Web.Util
{
    /// <summary>
    /// Http连接操作帮助类
    ///<see cref="https://www.cnblogs.com/johnyong/p/13296793.html"/>
    /// </summary>
    public class HttpHelper
    {
        private static HttpHelper instance = null;
        public static HttpHelper Getinstance()
        {
            if (instance == null)
            {
                instance = new HttpHelper();
            }
            return instance;
        }
        /// <summary>
        /// 注入http请求
        /// </summary>
        private readonly IHttpClientFactory httpClientFactory;
        public HttpHelper()
        {
            httpClientFactory = ConfigHelper.GetService<IHttpClientFactory>();
        }
        public HttpHelper(IHttpClientFactory _httpClientFactory)
        {
            httpClientFactory = _httpClientFactory;
        }
        // <summary>
        // Get请求数据.弃用
        // <para>最终以url参数的方式提交</para>
        // </summary>
        // <param name="parameters">参数字典,可为空</param>
        // <param name="requestUri">例如/api/Files/UploadFile</param>
        // <returns></returns>
        public async Task<string> Get(string requestUri, Dictionary<string, string> parameters, string token)
        {
            //从工厂获取请求对象
            var client = BuildHttpClient(null, 180);
            //添加请求头
            if (!string.IsNullOrWhiteSpace(token))
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
            //这句加上会报Misused header name. Make sure request headers are used with HttpRequestMess 
            //client.DefaultRequestHeaders.Add("Content-Type", "application/json;charset=utf-8");   
            //拼接地址
            if (parameters != null)
            {
                var strParam = string.Join("&", parameters.Select(o => o.Key + "=" + o.Value));
                var seperator = requestUri.Contains("?") ? "&" : "?";//判断是否已经带了参数
                requestUri = string.Concat(requestUri, seperator, strParam);
            }
            //client.BaseAddress = new Uri(requestUri);
            return await client.GetStringAsync(requestUri);
            //return  client.GetStringAsync(requestUri).Result;
        }
        public async Task<T> GetAsync<T>(string requestUri, Dictionary<string, string> headers, Dictionary<string, string> parameters, int timeoutSecond = 120)
        {
            try
            {
                //从工厂获取请求对象
                var client = BuildHttpClient(headers, timeoutSecond);           
                //这句加上会报Misused header name. Make sure request headers are used with HttpRequestMess 
                //client.DefaultRequestHeaders.Add("Content-Type", "application/json;charset=utf-8");   
                //拼接地址参数
                if (parameters != null)
                {
                    var strParam = string.Join("&", parameters.Select(o => o.Key + "=" + o.Value));
                    var seperator = requestUri.Contains("?") ? "&" : "?";//判断是否已经带了参数
                    requestUri = string.Concat(requestUri, seperator, strParam);
                }
                var request = new HttpRequestMessage(HttpMethod.Get, requestUri);
                //client.BaseAddress = new Uri(requestUri);
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(responseContent);
                    return result;
                }
                else
                {
                    NLogHelper.logger.Info($"接口请求错误,错误代码{response.StatusCode}，错误原因{response.ReasonPhrase}");
                    throw new MyHttpException(response.StatusCode.ToString(), response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                NLogHelper.logger.Info($"接口请求错误,HttpGet:{requestUri} Error:{ex.ToString()}");
                throw new Exception($"HttpGet:{requestUri} Error", ex);
            }
        }
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="url"></param>
        /// <param name="dicHeaders"></param>
        /// <param name="timeoutSecond"></param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(string url, Dictionary<string, string> dicHeaders, int timeoutSecond = 180)
        {
            try
            {
                var client = BuildHttpClient(dicHeaders, timeoutSecond);
                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(responseContent);
                    return result;
                }
                else
                {
                    NLogHelper.logger.Error($"接口请求错误,错误代码{response.StatusCode}，错误原因{response.ReasonPhrase}");
                    throw new MyHttpException(response.StatusCode.ToString(), response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                NLogHelper.logger.Info($"接口请求错误,HttpGet:{url} Error:{ex.ToString()}");
                throw new Exception($"HttpGet:{url} Error", ex);
            }
        }

        /// <summary>
        /// POST
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="requestBody">请求体</param>
        /// <param name="headers"></param>
        /// <param name="timeoutSecond"></param>
        /// <returns></returns>
        public async Task<T> PostAsync<T>(string url, Dictionary<string, string> headers, Dictionary<string, string> requestBody, int timeoutSecond = 120)
        {
            string requestString = "";
            //拼接地址参数
            if (requestBody != null)
            {
                requestString = string.Join("&", requestBody.Select(o => o.Key + "=" + o.Value));
            }
            return await PostAsync<T>(url, headers, requestString, timeoutSecond);
        }
        /// <summary>
        /// Post
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="dicHeaders"></param>
        /// <param name="timeoutSecond"></param>
        /// <returns></returns>
        public async Task<T> PostAsync<T>(string url, Dictionary<string, string> dicHeaders, string requestBody, int timeoutSecond = 180)
        {
            try
            {
                var client = BuildHttpClient(null, timeoutSecond);
                var requestContent = GenerateStringContent(requestBody, dicHeaders);
                var response = await client.PostAsync(url, requestContent);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(responseContent);
                    return result;
                }
                else
                {
                    NLogHelper.logger.Error($"接口请求错误,错误代码{response.StatusCode}，错误原因{response.ReasonPhrase}");
                    throw new MyHttpException(response.StatusCode.ToString(), response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                NLogHelper.logger.Info($"接口请求错误,HttpPost:{url},body:{requestBody} Error:{ex.ToString()}");
                throw new Exception($"HttpPost:{url} Error", ex);
            }
        }

        /// <summary>
        /// Put
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="dicHeaders"></param>
        /// <param name="timeoutSecond"></param>
        /// <returns></returns>
        public async Task<T> PutAsync<T>(string url, Dictionary<string, string> dicHeaders, string requestBody, int timeoutSecond = 180)
        {
            try
            {
                var client = BuildHttpClient(null, timeoutSecond);
                var requestContent = GenerateStringContent(requestBody, dicHeaders);
                var response = await client.PutAsync(url, requestContent);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(responseContent);
                    return result;
                }
                else
                {
                    NLogHelper.logger.Error($"接口请求错误,错误代码{response.StatusCode}，错误原因{response.ReasonPhrase}");
                    throw new MyHttpException(response.StatusCode.ToString(), response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                NLogHelper.logger.Info($"接口请求错误,HttpPut:{url},Body:{requestBody},  Error:{ex.ToString()}");
                throw new Exception($"HttpPut:{url} Error", ex);
            }
        }


        /// <summary>
        /// Patch
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="dicHeaders"></param>
        /// <param name="timeoutSecond"></param>
        /// <returns></returns>
        public async Task<T> PatchAsync<T>(string url, Dictionary<string, string> dicHeaders, string requestBody, int timeoutSecond = 180)
        {
            try
            {
                var client = BuildHttpClient(null, timeoutSecond);
                var requestContent = GenerateStringContent(requestBody, dicHeaders);
                var response = await client.PatchAsync(url, requestContent);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(responseContent);
                    return result;
                }
                else
                {
                    NLogHelper.logger.Error($"接口请求错误,错误代码{response.StatusCode}，错误原因{response.ReasonPhrase}");
                    throw new MyHttpException(response.StatusCode.ToString(), response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                NLogHelper.logger.Info($"接口请求错误,HttpPatch:{url},body:{requestBody}, Error:{ex.ToString()}");
                throw new Exception($"HttpPatch:{url} Error", ex);
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="url"></param>
        /// <param name="dicHeaders"></param>
        /// <param name="timeoutSecond"></param>
        /// <returns></returns>
        public async Task<T> DeleteAsync<T>(string url, Dictionary<string, string> dicHeaders, int timeoutSecond = 180)
        {
            try
            {
                var client = BuildHttpClient(dicHeaders, timeoutSecond);
                var response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(responseContent);
                    return result;
                }
                else
                {
                    NLogHelper.logger.Error($"接口请求错误,错误代码{response.StatusCode}，错误原因{response.ReasonPhrase}");
                    throw new MyHttpException(response.StatusCode.ToString(), response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                NLogHelper.logger.Info($"接口请求错误,HttpDelete:{url}, Error:{ex.ToString()}");
                throw new Exception($"HttpDelete:{url} Error", ex);
            }
        }
        /// <summary>
        /// 通用异步请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="requestBody"></param>
        /// <param name="dicHeaders"></param>
        /// <param name="timeoutSecond">默认180秒</param>
        /// <returns></returns>
        public async Task<T> RequestAsync<T>(string url, Dictionary<string, string> dicHeaders, HttpMethod method, string requestBody, int timeoutSecond = 180)
        {
            try
            {
                var client = BuildHttpClient(null, timeoutSecond);
                var request = GenerateHttpRequestMessage(url, requestBody, method, dicHeaders);
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var type = typeof(T);
                    if (type.IsPrimitive || type == typeof(string))
                    {
                        return (T)Convert.ChangeType(responseContent, typeof(T));
                    }
                    else
                    {
                        return JsonConvert.DeserializeObject<T>(responseContent);
                    }
                }
                else
                {
                    NLogHelper.logger.Error($"接口请求错误,错误代码{response.StatusCode}，错误原因{response.ReasonPhrase}");
                    throw new MyHttpException(response.StatusCode.ToString(), response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                NLogHelper.logger.Info($"接口请求错误,{method.ToString()}:{url},body:{requestBody}, Error:{ex.ToString()}");
                throw new Exception($"{method.ToString()}:{url} Error", ex);
            }

        }
        /// <summary>
        /// Build HttpClient
        /// </summary>
        /// <param name="dicDefaultHeaders"></param>
        /// <param name="timeoutSecond"></param>
        /// <returns></returns>
        private HttpClient BuildHttpClient(Dictionary<string, string> dicDefaultHeaders, int? timeoutSecond)
        {
            var httpClient = httpClientFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Clear();   //in order that the client is not affected by the last request,it need to clear DefaultRequestHeaders
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("User-Agent", GetUserAgent());
            httpClient.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

            if (dicDefaultHeaders != null)
            {
                foreach (var headerItem in dicDefaultHeaders)
                {
                    if (!httpClient.DefaultRequestHeaders.Contains(headerItem.Key))
                    {
                        httpClient.DefaultRequestHeaders.Add(headerItem.Key, headerItem.Value);
                    }
                }
            }
            if (timeoutSecond != (int?)null)
            {
                httpClient.Timeout = TimeSpan.FromSeconds(timeoutSecond.Value);
            }
            return httpClient;
        }

        /// <summary>
        /// Generate HttpRequestMessage
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="method"></param>
        /// <param name="dicHeaders"></param>
        /// <returns></returns>
        private HttpRequestMessage GenerateHttpRequestMessage(string url, string requestBody, HttpMethod method, Dictionary<string, string> dicHeaders)
        {
            var request = new HttpRequestMessage(method, url);
            if (!string.IsNullOrEmpty(requestBody))
            {
                request.Content = new StringContent(requestBody);
            }
            if (dicHeaders != null)
            {
                foreach (var header in dicHeaders)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }
            return request;
        }
        /// <summary>
        ///  Generate StringContent
        /// </summary>
        /// <param name="requestBody"></param>
        /// <param name="dicHeaders"></param>
        /// <returns></returns>
        private StringContent GenerateStringContent(string requestBody, Dictionary<string, string> dicHeaders)
        {
            var content = new StringContent(requestBody);
            if (dicHeaders != null)
            {
                foreach (var headerItem in dicHeaders)
                {
                    content.Headers.Add(headerItem.Key, headerItem.Value);
                }
            }
            return content;
        }
        /// <summary>
        /// 用户代理
        /// </summary>
        /// <returns></returns>
        private static string GetUserAgent()
        {
            var userAgents = new List<string>
             {
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
                "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
                 "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
                 "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
                 "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
                 "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
                 "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
                 "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
                 "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
                 "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
                 "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
                 "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
                 "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
                 "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
                 "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
                 "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
                 "Mozilla/5.0 (Macintosh; U; Mac OS X Mach-O; en-US; rv:2.0a) Gecko/20040614 Firefox/3.0.0 ",
                 "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.0.3) Gecko/2008092414 Firefox/3.0.3",
                "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5",
                 "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 AlexaToolbar/alxf-2.0 Firefox/3.6.14",
                 "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
                "Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.11",
                 "Opera/9.80 (Android 2.3.4; Linux; Opera mobi/adr-1107051709; U; zh-cn) Presto/2.8.149 Version/11.10",
                 "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10",
                 "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8",
                 "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5",
                 "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0",
                 "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)",
                 "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)",
                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"
            };
            return userAgents.OrderBy(x => Guid.NewGuid()).First();
        }
    }
    public class MyHttpException : Exception
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public MyHttpException() : base()
        { }
        public MyHttpException(string errorCode, string message) : base(message)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = ErrorMessage;
        }
    }

}