﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Oauth2.impl
{
    public class OSChinaConnector : OauthConnector
    {
        public OSChinaConnector(String appkey, String appSecret) : base(appkey, appSecret)
        {
            Type = OauthType.oschina;
        }

        protected override String CreateAuthorizeUrl(String state)
        {
            StringBuilder urlBuilder = new StringBuilder("http://www.oschina.net/action/oauth2/authorize?");
            urlBuilder.Append("response_type=code");
            urlBuilder.Append("&client_id=" + ClientId);
            urlBuilder.Append("&redirect_uri=" + RedirectUri);
            urlBuilder.Append("&state=" + state);

            return urlBuilder.ToString();
        }

        protected override OauthUser GetOauthUser(String code)
        {
            String accessToken = GetAccessToken(code).access_token;

            String url = "http://www.oschina.net/action/openapi/user?access_token=" + accessToken + "&dataType=json";

            String httpString =HttpGet(url);
            if (String.IsNullOrEmpty(httpString))
            {
                return null;
            }
            // {"gender":"male","name":"michaely","location":"北京 朝阳","id":111634,
            // "avatar":"http://static.oschina.net/uploads/user/55/111634_50.jpg?t=1414374101000",
            // "email":"fuhai999@gmail.com","url":"http://my.oschina.net/yangfuhai"}

            var jObject = JObject.Parse(httpString);
            OauthUser user = new OauthUser();
            user.Avatar= jObject["avatar"].ToString();
            user.OpenId= jObject["id"].ToString();
            user.Nickname= jObject["name"].ToString();
            user.Gender= jObject["gender"].ToString();
            user.Source=Type.ToString();

            return user;
        }

        public override AccessToken GetAccessToken(String code)
        {

            StringBuilder urlBuilder = new StringBuilder("http://www.oschina.net/action/openapi/token?");
            urlBuilder.Append("grant_type=authorization_code");
            urlBuilder.Append("&dataType=json");
            urlBuilder.Append("&client_id=" + ClientId);
            urlBuilder.Append("&client_secret=" + ClientSecret);
            urlBuilder.Append("&redirect_uri=" + RedirectUri);
            urlBuilder.Append("&code=" + code);

            String url = urlBuilder.ToString();

            String httpString = HttpGet(url);
            // {"access_token":"07a2aeb2-0790-4a36-ae24-40b90c4fcfc1",
            // "refresh_token":"bfd67382-f740-4735-b7f0-86fb9a2e49dc",
            // "uid":111634,
            // "token_type":"bearer",
            // "expires_in":604799}

            var jObject = JObject.Parse(httpString);
            return new AccessToken( jObject["access_token"].ToString());
        }

        public override AccessToken RefreshAccessToken(string refresh_token)
        {
            throw new NotImplementedException();
        }
    }
}
