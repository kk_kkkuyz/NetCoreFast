﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Oauth2.impl;
using static Web.Oauth2.OauthConnector;

namespace Web.Oauth2
{
    public class OauthConnectorFactory
    {
        static Dictionary<string, OauthConnector> oauths = new Dictionary<string, OauthConnector>();
        public static OauthConnector Build(String name, String appkey, String appSecret)
        {
            //OauthConnector connector = oauths.ContainsKey(name);
            OauthConnector connector=null;
            if (!oauths.ContainsKey(name))
            {
                if (OauthType.baidu.ToString() == name)
                {
                    connector = new BaiduConnector(appkey, appSecret);
                }
                if (OauthType.qq.ToString() == name)
                {
                    connector = new QQConnector(appkey, appSecret);
                }
                else if (OauthType.wechat.ToString() == name)
                {
                    connector = new WechatConnector(appkey, appSecret);
                }
                else if (OauthType.weibo.ToString() == name)
                {
                    connector = new WeiboConnector(appkey, appSecret);
                }
                else if (OauthType.oschina.ToString() == name)
                {
                    connector = new OSChinaConnector(appkey, appSecret);
                }
                else if (OauthType.github.ToString() == name)
                {
                    connector = new GithubConnector(appkey, appSecret);
                }
                else if (OauthType.facebook.ToString() == name)
                {
                    connector = new FacebookConnector(appkey, appSecret);
                }
                else if (OauthType.twitter.ToString() == name)
                {
                    connector = new TwitterConnector(appkey, appSecret);
                }
                else if (OauthType.linkedin.ToString() == name)
                {
                    connector = new LinkedinConnector(appkey, appSecret);
                }
                if (connector != null)
                {
                    oauths.Add(name, connector);
                }
            }
            else
            {
                connector = oauths[name];
            }

            return connector;
        }

    }
}
