/* author:QinYongcheng */


using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;  
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Sys;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
using Model.Entity.Sys;
using Model.Dto.Sys;
using Web.Security;

namespace Web.Controllers.Sys  
{
    /// <summary>
    /// 用户
    /// <p>模块名称:<b>Sys</b></p>
    /// <p>实体类名:<b>SysUser</b></p>
    /// </summary>
    [ApiController]
    [Route("api/Sys/[controller]/[action]")]
    [Authority(Module ="Sys")]
    public class SysUserController : MyApiBaseController<SysUser, SysUserListDto, SysUserAddDto, SysUserUpdateDto>
    {
        
        
        public SysUserController(ISysUserBll bll) : base(bll)
        {
        }
       
       
        
       
        
        
    }
}
