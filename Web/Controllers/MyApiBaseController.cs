﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLL;
using Common;
using Web.Extension;
using Web.Filter;
using AutoMapper;
using Model.Entity;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Web.Controllers
{
    //[ApiController]
    //[Route("api/[controller]/[action]")]
    [QueryFilter]
    //[Authority]//基于角色>权限>资源的权限控制
    //[Authorize("admin")]  基于角色策略>角色的权限控制
    public abstract class MyApiBaseController<IDEntity, TListDto, TAddDto, TUpdateDto> : MyEmptyApiBaseController<IDEntity, TListDto, TAddDto, TUpdateDto> where TUpdateDto : IID where IDEntity : ID, new()
    {
        public MyApiBaseController(IBaseBll<IDEntity> bll) : base(bll) { }
        /// <summary>
        /// 分页列表查询
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**模块名_实体类名_list**</br> 
        /// <br>查询条件格式：api/实体名/List?sort=-Id&amp;name=武汉&amp;pageNo=1 </br>
        /// <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-Id根据Id字段递减排序；+Id根据Id字段递增排序。可多个字段排序：如+Id,-Added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
        /// <br>pageNo：查询页码；&lt;=0则查询所有，默认1。</br>
        /// <br>pageSize：每页显示数量；默认12。</br>
        /// <br>条件查询，格式：字段名__操作符=值。如"parent.id__eq=1"、 ParentId=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
        /// <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,ge大于等于,lt小于, le小于等于,in包含。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
        /// </remarks>
        /// <param name="where">此参数用于根据查询字符串自动组装查询条件字符串参数，无须人工干预传值</param>
        /// <param name="excludes">排除要查询那些字段(可选)，比如提高查询效率，排除导航属性（即对象属性）</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<Result<Pagination<TListDto>>> List([FromQuery] Dictionary<string, string> where, String excludes = null, CancellationToken token = default)
        {
            // mapper.Map<TListDto>(await bll.QueryAsync(where, excludes));
            return Result<Pagination<TListDto>>.Success("succeed").SetData(await bll.QueryToAsync<TListDto>(where, excludes, token));
        }
        /// <summary>
        /// 查询详情
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**模块名_实体类名_get**</br> 
        /// 格式:api/实体名/Get/id值，如：api/role/1
        /// </remarks>
        /// <param name="id">要查询的id值，放在路径中</param>
        /// <param name="where">查询其他条件，条件格式与list查询的条件一致。此参数用于根据查询字符串自动组装查询条件字符串参数，无须人工干预传值</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public virtual async Task<Result<TUpdateDto>> Get(int id, [FromQuery] Dictionary<string, string> where, CancellationToken token = default)
        {
            return Result<TUpdateDto>.Success("succeed").SetData(await bll.SelectOneToAsync<TUpdateDto>(where, null, true, token));
        }
        /// <summary>
        /// 添加实体对象
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**模块名_实体类名_add**</br> 
        /// </remarks>
        /// <param name="o">实体对象不需要id属性值</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Result<object>> Add(TAddDto o, CancellationToken token = default)
        {
            return ModelState.IsValid ? (await bll.AddAsync(mapper.Map<TAddDto, IDEntity>(o), token) ? Result<object>.Success("添加成功") : Result<object>.Error("添加失败")) : Result<object>.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }
        /// <summary>
        /// 修改实体对象
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**模块名_实体类名_update**</br> 
        /// </remarks>
        /// <param name="o">实体对象必须有id属性，根据id修改实体对象</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpPost]
        public virtual async Task<Result<object>> Update(TUpdateDto o, CancellationToken token = default)
        {
            return ModelState.IsValid ? (await bll.UpdateAsync<TUpdateDto>(o) ? Result<object>.Success("修改成功").SetData(o) : Result<object>.Error("修改失败")) : Result<object>.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }
        /// <summary>
        /// 删除实体对象
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**模块名_实体类名_delete**</br> 
        /// <br>（1）根据id单个删除，如：/api/Area/Delete/id值或/api/Area/Delete?id=1</br>
        /// <br>（2）根据id批量删除，如：/api/Area/Delete/1,2,3 或/apiArea/Delete?id = 1,2,3  多个id值用“,”隔开</br>
        /// <br>（3）条件批量删除，如：/api/Area/Delete?name=武汉  ，条件格式与list查询的条件一致</br>
        /// </remarks> 
        /// <param name="id">id值，单个值如：1，多个值如：1,2,3</param>
        /// <param name="where">查询其他条件，条件格式与list查询的条件一致。此参数用于根据查询字符串自动组装查询条件字符串参数，无须人工干预传值</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public virtual async Task<Result<object>> Delete(string id, [FromQuery] Dictionary<string, string> where, CancellationToken token = default)
        {
            return await bll.DeleteAsync(where, token) ? Result<object>.Success("删除成功") : Result<object>.Error("删除失败");
        }
        /// <summary>
        /// 批量删除实体对象
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**模块名_实体类名_batchdelete**</br> 
        /// <br>如：api/Area/BatchDelete?name=武汉 </br>
        /// <br>注：条件格式与list查询的条件一致</br>
        /// </remarks>
        /// <param name="where">查询其他条件，条件格式与list查询的条件一致。此参数用于根据查询字符串自动组装查询条件字符串参数，无须人工干预传值</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<Result<object>> BatchDelete([FromQuery] Dictionary<string, string> where, CancellationToken token = default)
        {
            return await bll.DeleteAsync(where, token) ? Result<object>.Success("删除成功") : Result<object>.Error("删除失败");
        }
    }
}