﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLL;
using Common;
using Web.Extension;
using Web.Filter;
using AutoMapper;
using Model.Entity;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Web.Controllers
{
    //[ApiController]
    //[Route("api/[controller]/[action]")]
    [QueryFilter]
    //[Authority]//基于角色>权限>资源的权限控制
    //[Authorize("admin")]  基于角色策略>角色的权限控制
    public abstract class MyEmptyApiBaseController<IDEntity, TListDto, TAddDto, TUpdateDto> : MyBaseController<IDEntity> where TUpdateDto : IID where IDEntity : ID, new()
    {
        public IMapper mapper { get; set; }
        protected readonly IBaseBll<IDEntity> bll;
        public MyEmptyApiBaseController(IBaseBll<IDEntity> bll)
        {
            this.bll = bll;
        }      
    }
}