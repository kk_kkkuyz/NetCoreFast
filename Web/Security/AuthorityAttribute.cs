﻿using Common;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using Model.Entity.Sys.Enum;
using Newtonsoft.Json;
using System.Linq;
using System.Security.Claims;
using Web.Extension;

namespace Web.Security
{
    /// <summary>
    /// 权限访问类型
    /// </summary>
    public enum AccessType
    {
        /// <summary>
        /// 需要有访此资源的权限，默认值
        /// </summary>
        Res = 0,
        /// <summary>
        /// 登录就能访问此资源的权限
        /// </summary>
        Login = 1,
    }
    /// <summary>
    ///权限验证。controller_action即资源名称
    /// </summary>
    public class AuthorityAttribute : ActionFilterAttribute
    {    
        /// <summary>
        /// 模块名称
        /// </summary>
        public string Module { get; set; }
        /// <summary>
        /// 权限访问类型. Res:需要有访此资源的权限，默认值。Login:登录就能访问此资源的权限
        /// </summary>
        public AccessType Access { get; set; } = AccessType.Res;
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {
                ReturnResult(context, false, StatusCodes.Status401Unauthorized, "很抱歉,您未登录！");
                return;
            }
            //登录就能访问的权限，就不需要判断是否有访问此资源的权限
            if(Access == AccessType.Login)
            {
                base.OnActionExecuting(context);
                return;
            }
            //当角色为admin时（超级管理员），不用验证权限
            //角色
            var role = context.HttpContext?.User?.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;
            if (string.IsNullOrEmpty(role))
            {
                ReturnResult(context, false, StatusCodes.Status403Forbidden, "您无角色,不能访问！");
                return;
            }
            if (role == RoleType.admin.ToString())//超级管理员
            {
                base.OnActionExecuting(context);
                return;
            }
            //用户id
            /*
            var userIdStr = context.HttpContext?.User?.Claims.FirstOrDefault(c => c.Type == JwtClaimTypes.Id)?.Value;
            int userId = 0;
            int.TryParse(userIdStr, out userId);          
            */
            var controller = context.ActionDescriptor.RouteValues["controller"].ToString();
            var action = context.ActionDescriptor.RouteValues["action"].ToString();
            var method = context.HttpContext.Request.Method;
            if (string.IsNullOrEmpty(controller) || string.IsNullOrEmpty(action) || string.IsNullOrEmpty(method))
            {
                ReturnResult(context, false, StatusCodes.Status403Forbidden, "controller or action or method is not found");
                return;
            }
            IDistributedCache cache = context.HttpContext.RequestServices.GetRequiredService(typeof(IDistributedCache)) as IDistributedCache;
            //从缓存获取角色对应资源
            var roleResMap = cache.Get<RoleResMap>(RoleResMap.CACHE_KEY);
            var resval = string.IsNullOrWhiteSpace(Module) ? $"{controller}_{action}" : $"{Module}_{controller}_{action}";//资源值
            if (!roleResMap.RoleHasRes(role, resval))
            {
                ReturnResult(context, false, StatusCodes.Status401Unauthorized, $"很抱歉,您无{resval}操作权限！请向管理员申请！");
                return;
            }
            base.OnActionExecuting(context);
        }
        private static void ReturnResult(ActionExecutingContext context, bool status, int statusCodes, string msg)
        {
            context.HttpContext.Response.ContentType = "application/json;charset=utf-8";
            var setting = new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };
            context.Result = new JsonResult(Result<object>.Instance(status, statusCodes, msg), setting);
        }

    }

}
