using Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using Web.Autofac;
using Web.DB;
using Model;
using Autofac;
using Microsoft.AspNetCore.Http.Json;
using Microsoft.Extensions.DependencyInjection;
using Plugin;

//https://www.jianshu.com/p/d6e15aba0526
//netcore3.1升级到net6.0 https://docs.microsoft.com/zh-cn/aspnet/core/migration/50-to-60?view=aspnetcore-6.0&tabs=visual-studio#smhm
var builder = WebApplication.CreateBuilder(args);
var startup = new Startup(builder.Configuration);
startup.ConfigureServices(builder.Services);

//https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/min-web-api?view=aspnetcore-6.0&tabs=visual-studio

//Using a custom DI container.
//builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.UseAutofacServiceProviderFactory();//将默认ServiceProviderFactory指定为AutofacServiceProviderFactory
builder.Host.ConfigureContainer<ContainerBuilder>(startup.ConfigureContainer);

//设置自定义修改启动端口配置文件
//https://www.jb51.net/article/240699.htm
//var configuration = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory).AddJsonFile("host.json").Build();
//builder.WebHost.UseConfiguration(configuration);   改为appsettings.json 文件中设置 Kestrel 端口：
var app = builder.Build();
startup.Configure(app, app.Environment);
app.InitDB<MyDbContext>();
app.UsePlugins();//添加插件支持
app.Run();
/*
//https://www.5axxw.com/questions/content/ukt4wg
//https://docs.microsoft.com/zh-cn/aspnet/core/web-api/advanced/formatting?view=aspnetcore-6.0
//设置自定义修改启动端口配置文件
var configuration = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory).AddJsonFile("host.json").Build();
Host.CreateDefaultBuilder(args)
    //.ConfigureServices(sc => sc.AddHostedService<LifetimeEventsHostedService>())
    .UseAutofacServiceProviderFactory()//将默认ServiceProviderFactory指定为AutofacServiceProviderFactory
        .ConfigureWebHostDefaults(webBuilder =>
        {
            //应用配置文件
            webBuilder.UseConfiguration(configuration).UseStartup<Startup>();
        })
        .Build().InitDB<MyDbContext>().Run();//运行项目，执行本语句;
*/