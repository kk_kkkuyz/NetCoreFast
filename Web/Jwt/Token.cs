﻿
namespace Web.Jwt
{
    /// <summary>
    /// 令牌
    /// </summary>
    public class Token
    {
        /// <summary>
        /// 登录用户Id
        /// </summary>
        public int  Uid { get; set; }
        /// <summary>
        /// 登录用户名
        /// </summary>
        public string Uname { get; set; }
        /// <summary>
        /// 身份角色
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// 其他项目信息
        /// </summary>
        public string Project { get; set; }
        /// <summary>
        /// 令牌类型（终端类型）
        /// </summary>
        public TokenType TokenType { get; set; }
        /// <summary>
        /// 租户id，比如公司id，学校id，用户id等等都可以作为租户id
        /// </summary>
        public int? TenantId { get; set; } = 0;
        /// <summary>
        /// 身份类型
        /// </summary>
        public string Type { get; set; }
        //其他自定义属性写在下面：
        /// <summary>
        /// 班级id
        /// </summary>
        public int? ClasssId { get; set; } = 0;
    
       
    }
}
