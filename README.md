# .Net6 Core脚手架，拿来即用

## 介绍
基于EF Core之Code First模式的NetCore快速开发框架。本脚手架支持代码自动生成，以快速开发.Net Core Api接口，实现前后端分离，支持开发多租户系统。极简至上，拒绝厚重。
**NET6版本努力让您有飞一般的体验!!!**
     
**！！！强烈建议通过[CodeGen](doc/InitProject.md)代码生成器[初始化项目](doc/InitProject.md)！！！**

## 配套前端
- 后台管理前端解决方案 [elementplus-admin-codegen](https://gitee.com/qinyongcheng/elementplus-admin-codegen)
- 移动端/微信端（uniapp解决方案） [uniapp-fast-colorUI](https://gitee.com/qinyongcheng/uniapp-fast-colorUI)

## 软件架构
- Asp.net core 6.0+EF Core 6.0(Code First模式)
- Newtonsoft.Json序列化
- AutoMapper 映射
- JWT+IdentityModel权限控制
- Autofac依赖注入
- Swagger+Swashbuckle.AspNetCore.ReDoc API文档
- StackExchange.Redis/csredis
- Quartz定时器
- DotNetCore.NPOI：word/excel支持
- NLog日志系统
- hyjiacan.py4n 汉子转拼音
- QRCoder 二维码生成器
- Mustachio模板引擎代码生成器
- SkiaSharp .NET 平台的跨平台 2D 图形 API
- Zack.EFCore.Batch [高性能批量操作库](https://github.com/yangzhongke/Zack.EFCore.Batch)
- System.Linq.Dynamic.Core [Linq动态查询库](https://github.com/zzzprojects/System.Linq.Dynamic.Core)
- 其他：ChakraCore.NET JavaScript引擎

## 开发环境
- VS2022 / net core 6.0

## 支持数据库
- SQL Server
- MySQL
- Sqlite

## 使用教程
#### 1. [初始项目](doc/InitProject.md)
#### 2. [快速开发](doc/Base.md)
#### 3. [自动生成类代码扩展](doc/Extend.md)
#### 4. [权限控制](doc/Authority.md)
#### 5. [登录与请求、获取用户信息](doc/Login.md)
#### 6. [查询过滤器](doc/Query.md)
#### 7. [定时器任务Quartz](doc/Quartz.md)
#### 8. [Excel导入与导出](doc/Excel.md) 
#### 9. [websocket使用](doc/Websocket.md)
#### 10. [升级项目](doc/UpdateProject.md)
## 目录结构
  Model：模型层   
  DAL：数据访问层   
  BLL：业务逻辑层  
  Web：控制器层   
  ImCore：基于Redis的即时聊天websocket核心
## 技术交流群
QQ：240131047
## 参考文档
[码云Markdown语法](https://gitee.com/oschina/team-osc/blob/master/markdown.md)  
[码云commonmark语法](https://commonmark.org/help/)

