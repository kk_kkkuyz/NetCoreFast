/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entity.Sys;
namespace BLL.Sys 
{
   
    public interface ISysUserBll:IBaseBll<SysUser>
    {
       
        /// <summary>
        /// 用户名密码登录
        /// </summary>
        public SysUser Login(string nuserame, string pswd);     

     
    }
}
