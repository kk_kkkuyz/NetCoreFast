/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysDocumentBll : BaseBll<SysDocument>, ISysDocumentBll
    {
        public SysDocumentBll(IBaseDAL<SysDocument> dal):base(dal)
        {           
        }
        
    }
}
