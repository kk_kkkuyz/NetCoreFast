/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysVisitRecordBll : BaseBll<SysVisitRecord>, ISysVisitRecordBll
    {
        public SysVisitRecordBll(IBaseDAL<SysVisitRecord> dal):base(dal)
        {           
        }
        
    }
}
