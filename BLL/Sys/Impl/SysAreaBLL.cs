/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysAreaBll : BaseBll<SysArea>, ISysAreaBll
    {
        public SysAreaBll(IBaseDAL<SysArea> dal):base(dal)
        {           
        }
        
    }
}
