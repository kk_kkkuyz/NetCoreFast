/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysAuthorityResBll : BaseBll<SysAuthorityRes>, ISysAuthorityResBll
    {
        public SysAuthorityResBll(IBaseDAL<SysAuthorityRes> dal):base(dal)
        {           
        }
        
    }
}
