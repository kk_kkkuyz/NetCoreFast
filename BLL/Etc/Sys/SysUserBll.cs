﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using Common.Util;
using Model.Entity.Sys;
using Common.MyAttribute;

namespace BLL.Etc.Sys
{
    /// <summary>
    /// 扩展bll。用于合并到自动生成的对应Bll类。
    /// </summary>
    [NotAutofac]//表示不会注入到容器，仅仅作为合并代码
    public interface ISysUserBll
    {
        /// <summary>
        /// 用户名密码登录
        /// </summary>
        public SysUser Login(string nuserame, string pswd);     

    }
    /// <summary>
    /// 扩展bll。用于合并到自动生成的对应Bll类。
    /// </summary>
    [NotAutofac]
    public class SysUserBll : BaseBll<SysUser>, ISysUserBll

    {
        
        public new bool Add(SysUser o)
        {
            o.Pswd = Security.Md5(o.Pswd);
            return dal.Add(o);
        }
        public SysUser Login(string userame, string pswd)
        {
            pswd = Security.Md5(pswd);
            return dal.SelectOne(o => o.Username == userame && o.Pswd == pswd);
        }       
    }
}
