﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using Common.Util;
using Model.Entity.Sys;
using Common.MyAttribute;

namespace BLL.Etc.Sys
{
    /// <summary>
    /// 扩展bll。用于合并到自动生成的对应Bll类。
    /// </summary>
    [NotAutofac]
    public interface ISysconfigBll
    {
        /// <summary>
        /// 查询配置或初始化
        /// </summary>
        /// <returns></returns>
        Sysconfig SelectOneOrInit();
    }
    /// <summary>
    /// 扩展bll。用于合并到自动生成的对应Bll类。
    /// </summary>
    [NotAutofac]
    public class SysconfigBll : BaseBll<Sysconfig>, ISysconfigBll

    {
        public Sysconfig SelectOneOrInit()
        {
            Sysconfig sysconfig = SelectOne(o => o.Id > 0);
            if (sysconfig == null)//没有记录 初始化
            {
                sysconfig = new Sysconfig() { Sys = true };
                bool ret = this.Add(sysconfig);
                return sysconfig;
            }
            return sysconfig;
        }
    }
}
