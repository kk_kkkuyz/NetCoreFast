﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL;
using Common;
using Microsoft.EntityFrameworkCore;
using Common.MyAttribute;
using Model;
using Model.Entity;
using System.Linq.Dynamic.Core;
using DAL.Extension;
using System.Threading;
using Common.Util;

namespace BLL
{

    [NotAutofac]
    public class BaseBll<T> : IBaseBll<T> where T : ID, new()
    {
        public IBaseDAL<T> dal { get; set; }
        public BaseBll()
        {

        }
        public BaseBll(IBaseDAL<T> dal)
        {
            this.dal = dal;
        }
        public MyDbContext DbContext()
        {
            return dal.DbContext();
        }
        public bool Add(T entity)
        {
            return dal.Add(entity);
        }
        public async Task<bool> AddAsync(T entity, CancellationToken token = default)
        {
            return await dal.AddAsync(entity, token);
        }
        public bool Add(IEnumerable<T> entities)
        {
            return dal.Add(entities);
        }
        public async Task<bool> AddAsync(IEnumerable<T> entities, CancellationToken token = default)
        {
            return await dal.AddAsync(entities, token);
        }
        public bool Delete(T entity)
        {
            return dal.Delete(entity);
        }
        public async Task<bool> DeleteAsync(T entity, CancellationToken token = default)
        {
            return await dal.DeleteAsync(entity, token);
        }
        public bool Delete(int id)
        {
            return dal.Delete(id);
        }
        public async Task<bool> DeleteAsync(int id, CancellationToken token = default)
        {
            return await dal.DeleteAsync(id, token);
        }
        public bool Delete(Expression<Func<T, bool>> where)
        {
            return dal.Delete(where);
        }
        public async Task<bool> DeleteAsync(Expression<Func<T, bool>> where, CancellationToken token = default)
        {
            return await dal.DeleteAsync(where, token);
        }
        public bool Delete(Dictionary<string, string> where)
        {
            return dal.Delete(where);
        }
        public async Task<bool> DeleteAsync(Dictionary<string, string> where, CancellationToken token = default)
        {
            return await dal.DeleteAsync(where, token);
        }
        public bool Delete(QueryParam<T> where)
        {
            return dal.Delete(where);
        }
        public async Task<bool> DeleteAsync(QueryParam<T> where, CancellationToken token = default)
        {
            return await dal.DeleteAsync(where, token);
        }
        public bool Update(T entity, params string[] propertyNames)
        {
            return dal.Update(entity, propertyNames);
        }
        public async Task<bool> UpdateAsync(T entity, CancellationToken token = default, params string[] propertyNames)
        {
            return await dal.UpdateAsync(entity, token, propertyNames);
        }
        public bool Update(T o)
        {
            return dal.Update(o);
        }
        public async Task<bool> UpdateAsync(T o, CancellationToken token = default)
        {
            return await dal.UpdateAsync(o, token);
        }
        public async Task<bool> UpdateAsync<E>(E dto, CancellationToken token = default) where E : IID
        {
            return await dal.UpdateAsync(dto, token);
        }
        public T SelectOne(int id, bool? asNoTracking = false)
        {
            return dal.SelectOne(id, asNoTracking);
        }
        public async Task<T> SelectOneAsync(int id, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectOneAsync(id, asNoTracking, token);
        }
        public T SelectOne(Expression<Func<T, bool>> where, bool? asNoTracking = false)
        {
            return dal.SelectOne(where, asNoTracking);
        }
        public async Task<T> SelectOneAsync(Expression<Func<T, bool>> where, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectOneAsync(where, asNoTracking, token);
        }
        public T SelectOne(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false)
        {
            return dal.SelectOne(where, excludes, asNoTracking);
        }
        public T SelectOne(QueryParam<T> where, string excludes = null, bool? asNoTracking = false)
        {
            return dal.SelectOne(where,excludes, asNoTracking);
        }
        public async Task<T> SelectOneAsync(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectOneAsync(where, excludes, asNoTracking, token);
        }
        public async Task<T> SelectOneAsync(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectOneAsync(where, excludes, asNoTracking, token);
        }
        public async Task<R> SelectOneToAsync<R>(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectOneToAsync<R>(where, excludes, asNoTracking, token);
        }
        public async Task<R> SelectOneToAsync<R>(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectOneToAsync<R>(where, excludes, asNoTracking, token);
        }
        public Pagination<T> Query(Dictionary<string, string> where, string excludes = null)
        {
            return dal.Query(where, excludes);
        }
        public Pagination<T> Query(QueryParam<T> where, string excludes = null)
        {
            return dal.Query(where, excludes);
        }
        public async Task<Pagination<T>> QueryAsync(Dictionary<string, string> where, string excludes = null, CancellationToken token = default)
        {
            return await dal.QueryAsync(where, excludes, token);
        }
        public async Task<Pagination<T>> QueryAsync(QueryParam<T> where, string excludes = null, CancellationToken token = default)
        {
            return await dal.QueryAsync(where, excludes, token);
        }
        public async Task<Pagination<R>> QueryToAsync<R>(Dictionary<string, string> where, string excludes = null, CancellationToken token = default)
        {
            return await dal.QueryToAsync<R>(where, excludes, token);
        }
        public async Task<Pagination<R>> QueryToAsync<R>(QueryParam<T> where, string excludes = null, CancellationToken token = default)
        {
            return await dal.QueryToAsync<R>(where, excludes, token);
        }
        //多表分页查询
        public Pagination<E> Query<E>(IQueryable<E> linq, Dictionary<string, string> where, string excludes = null)
        {
            return dal.Query(linq, where, excludes);
        }
        public Pagination<E> Query<E>(IQueryable<E> linq, QueryParam<T> where, string excludes = null)
        {
            return dal.Query(linq, where, excludes);
        }
        public async Task<Pagination<E>> QueryAsync<E>(IQueryable<E> linq, Dictionary<string, string> where, string excludes = null, CancellationToken token = default)
        {
            return await dal.QueryAsync(linq, where, excludes, token);
        }
        public async Task<Pagination<E>> QueryAsync<E>(IQueryable<E> linq, QueryParam<T> where, string excludes = null, CancellationToken token = default)
        {
            return await dal.QueryAsync(linq, where, excludes, token);
        }
        public DbSet<T> Query()
        {
            return dal.Query();
        }
        public IEnumerable<T> SelectAll( bool? asNoTracking = false)
        {
            return dal.SelectAll(asNoTracking);
        }
        public async Task<IEnumerable<T>> SelectAllAsync( bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectAllAsync(asNoTracking, token);
        }
        public IEnumerable<T> SelectAll(Expression<Func<T, bool>> where, bool? asNoTracking = false)
        {
            return dal.SelectAll(where, asNoTracking);
        }
        public async Task<IEnumerable<T>> SelectAllAsync(Expression<Func<T, bool>> where, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectAllAsync(where, asNoTracking, token);
        }
        public IEnumerable<T> SelectAll(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false)
        {
            return dal.SelectAll(where, excludes, asNoTracking);
        }
        public IEnumerable<T> SelectAll(QueryParam<T> where, string excludes = null, bool? asNoTracking = false)
        {
            return dal.SelectAll(where, excludes, asNoTracking);
        }
        public async Task<IEnumerable<T>> SelectAllAsync(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectAllAsync(where, excludes, asNoTracking, token);
        }
        public async Task<IEnumerable<T>> SelectAllAsync(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectAllAsync(where, excludes, asNoTracking, token);
        }
        public Pagination<T> SelectAll(Expression<Func<T, bool>> where, int pageNo, int pageSize, bool? asNoTracking = false)
        {
            return dal.SelectAll(where, pageNo, pageSize, asNoTracking);
        }
        public async Task<Pagination<T>> SelectAllAsync(Expression<Func<T, bool>> where, int pageNo, int pageSize, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectAllAsync(where, pageNo, pageSize, asNoTracking, token);
        }
        public Pagination<T> SelectAll<OrderKey>(Expression<Func<T, bool>> whereLambda, Func<T, OrderKey> orderbyLambda, bool asc, int pageNo, int pageSize, bool? asNoTracking = false)
        {
            return dal.SelectAll(whereLambda, orderbyLambda, asc, pageNo, pageSize, asNoTracking);
        }
        public async Task<Pagination<T>> SelectAllAsync<OrderKey>(Expression<Func<T, bool>> whereLambda, Func<T, OrderKey> orderbyLambda, bool asc, int pageNo, int pageSize, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await dal.SelectAllAsync(whereLambda, orderbyLambda, asc, pageNo, pageSize, asNoTracking, token);
        }
    }
}
