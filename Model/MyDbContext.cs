﻿using System;
using Microsoft.EntityFrameworkCore;
using Common.Util;
using Model.Entity.Sys;
using Microsoft.Extensions.Options;
using Zack.EFCore.Batch;
namespace Model
{
    /// <summary>
    /// 初始化数据库、执行迁移等功能。
    /// 1、Add-Migration 版本名  //令创建迭代版本
    /// 2、Remove-Migration  删除迁移
    /// <see cref="https://learn.microsoft.com/zh-cn/ef/core/extensions/"/>
    /// </summary>
    public class MyDbContext : DbContext
    {
        //public MyDbContext()
        //{
        //}
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SysDepartment>(builder =>
            {
                //实体类多个导航属性的类型是同一个实体类的情况下
                builder.HasOne(e => e.User).WithMany();
                builder.HasOne(e => e.Leader).WithMany();
                //builder.HasMany(e => e.Recipients).WithMany(e => e.Notifications);
            });
            //modelBuilder.Entity<User>().HasData(
            //    new User {Id=1, Username = "admin", Pswd = Security.Md5("123456") });
            //    modelBuilder
            //.Entity<Goods>()
            //.Property(e => e.Photos)
            //.HasConversion(
            //    v => string.Join("|", v), 
            //    v => v.Split(new char[] { '|' })
            //    );
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }

        public virtual DbSet<SysArea> SysAreas { get; set; }
        public virtual DbSet<SysDocument> SysImages { get; set; }
        public virtual DbSet<SysUser> SysUsers { get; set; }
        public virtual DbSet<SysAuthority> SysAuthoritys { get; set; }
        public virtual DbSet<SysAuthorityRes> SysAuthorityRess { get; set; }
        public virtual DbSet<SysRes> SysRess { get; set; }
        public virtual DbSet<SysRole> SysRoles { get; set; }
        public virtual DbSet<SysRoleAuthority> SysRoleAuthoritys { get; set; }
        public virtual DbSet<Sysconfig> Sysconfigs { get; set; }
        public virtual DbSet<SysDepartment> SysDepartments { get; set; }
        public virtual DbSet<SysVisitRecord> SysVisitRecords { get; set; }
    }
}
