﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;

namespace Model.Entity
{
    public abstract class TreeID<E> : ID
    {
        public TreeID()
        {
            Childs = new HashSet<E>();
        }
        /// <summary>
        /// 上级ID
        /// </summary>
        public int? ParentId { get; set; }
        /// <summary>
        /// 上级对象， 客户端做增加修改时不需要给此属性赋值（只需要传递修改parentId属性值）
        /// </summary>
        [ForeignKey("ParentId")]
        [Display(Name = "上级")]
        //[JsonIgnore]
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true, NotList = true)]
        public virtual  E? Parent { get; set; }//virtual延迟加载与无跟踪在一起使用的时候就不能加载导航属性了。
        /// <summary>
        /// 上级名称
        /// </summary>
        [NotMapped]
        [FromModel("Parent.Name")]
        [DtoGen(NotAddUpdate = true)]
        public string ParentName { get; set; }
        /// <summary>
        /// 下级集合， 客户端做增加修改时不需要给此属性赋值（只需要传递修改UserId属性值）
        /// </summary>
       // [SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public  ICollection<E>? Childs { get; set; }
    }
}
