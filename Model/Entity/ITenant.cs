﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entity
{
    /// <summary>
    /// 标志多租户id
    /// </summary>
    public interface ITenant
    {
        /// <summary>
        /// 租户id。比如公司id，学校id，用户id等等都可以作为租户id
        /// </summary>
        public int TenantId { get; set; }
    }
}
