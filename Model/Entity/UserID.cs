﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Model.Entity.Sys;
using Common.MyAttribute;

namespace Model.Entity
{
    /// <summary>
    /// 统一 表示要记录当前这条记录是哪个用户添加的时候，可以继承 UserID或实现IUser
    /// </summary>
    public abstract class UserID : ID, IUser
    {
        /// <summary>
        /// 添加此记录的用户ID，系统自动赋值
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(AddUpdateButInvisible =true)] 
        public int? UserId { get; set; }
        /// <summary>
        /// 添加此记录的用户对象，客户端做增加修改时不需要给此属性赋值（只需要传递修改UserId属性值）
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true,NotList =true)]
        [ForeignKey("UserId")]
        public virtual  SysUser User { get; set; }////virtual延迟加载与无跟踪在一起使用的时候就不能加载导航属性了。
        /// <summary>
        /// 添加用户
        /// </summary>
        [NotMapped]
        [FromModel("User.Username")]
        [DtoGen(NotAddUpdate = true)]
        public string UserName { get; set; }
    }
}
