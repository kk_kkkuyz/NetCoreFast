﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.Entity
{
    /// <summary>
    /// 表示要记录当前这条记录是哪个用户添加的时候，可以继承 UserID或实现IUser
    /// </summary>
    public interface IUser
    {
        public int? UserId { get; set; }
    }
}
