﻿using Common.MyAttribute;
using Model.Entity.Sys;
using Model.Entity.Sys.Enum;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Model.Entity
{
    [Serializable]
    public abstract class Person : ID
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Required(ErrorMessage = "用户名必填")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "用户名长度3到20位")]
        public string Username { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        //[JsonIgnore]// 加了会导致接收不了参数
        [DtoGen(NotList = true, NotUpdate = true, NotDetail = true)]
        [Required(ErrorMessage = "密码必填")]
        [StringLength(40, MinimumLength = 6, ErrorMessage = "密码长度6到20位")]
        public string Pswd { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string? Realname { get; set; }//真实姓名
        /// <summary>
        ///         角色，可以多个角色。多个之间用,分割角色可以继承
        /// </summary>
        [Required(ErrorMessage = "角色必填")]
        public string Role { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [StringLength(15)]
        public string? Tel { get; set; }//电话
        /// <summary>
        /// 邮箱
        /// </summary>
        [StringLength(30)]
        public string? Email { get; set; }//电子邮箱
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime? Birthday { get; set; }//出生日期
        public string? QQ { get; set; }
        /// <summary>
        /// 联系地址
        /// </summary>
        public string? Address { get; set; }//联系地址
        /// <summary>
        /// 性别
        /// </summary>
        public Gender? Gender { get; set; }//性别
        /// <summary>
        /// 头像
        /// </summary>
        public string? Pic { get; set; }//头像
        /// <summary>
        /// 民族
        /// </summary>
        public string? Nation { get; set; }//民族
        /// <summary>
        /// 身份证号
        /// </summary>
        public string? IDCard { get; set; }//身份证号码
        /// <summary>
        /// 邮政编码
        /// </summary>
        public string? Zip { get; set; }//邮政编码
        /// <summary>
        /// 区域id
        /// </summary>
        public int? AreaId { get; set; }
        /// <summary>
        /// 区域对象。客户端做增加修改时不需要给此属性赋值（修改对应的areaId即可）
        /// </summary>
        [ForeignKey("AreaId")]
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public virtual SysArea Area { get; set; }//所在地区
        /// <summary>
        /// 区域名称
        /// </summary>
        [NotMapped]
        [FromModel("Area.Name")]
        [DtoGen(NotAddUpdate = true)]
        public string AreaName { get; set; }
        /// <summary>
        /// 可用预存款
        /// </summary>
        //[Column(TypeName = "decimal(18, 2)")]
        public double? Available_balance { get; set; } //可用预存款
        /// <summary>
        /// 冻结预存款
        /// </summary>
        //[Column(TypeName = "decimal(18, 2)")]
        public double? Freeze_blance { get; set; }//冻结预存款
        /// <summary>
        /// 积分
        /// </summary>
        public int? Integral { get; set; }//积分
        /// <summary>
        /// 金币
        /// </summary>
        public int? Gold { get; set; }//金币
        /// <summary>
        /// 好友数量
        /// </summary>
        public int? Friend_num { get; set; }//好友数
        /// <summary>
        /// 关注数
        /// </summary>
        public int? Attention_num { get; set; }//关注数
        /// <summary>
        /// 被关注数
        /// </summary>
        public int? Attentioned_num { get; set; }//被关注数
        /// <summary>
        /// 会员信用值
        /// </summary>
        public int? Credit { get; set; }//会员信用\
        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime? Last_login_date { get; set; }//最后登录时间
        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime? Login_date { get; set; }//登陆时间
        /// <summary>
        /// 最后登录ip
        /// </summary>
        public string? Last_login_ip { get; set; }//最后登录IP
        /// <summary>
        /// 登录ip
        /// </summary>
        public string? Login_ip { get; set; }//登录IP
        /// <summary>
        /// 登录次数
        /// </summary>
        public int? Login_count { get; set; }//登录次数
        /// <summary>
        /// 国际移动设备识别码
        /// </summary>
        public string? IMEI { get; set; }//国际移动设备识别码
        /// <summary>
        /// 紧急联系电话
        /// </summary>
        public string? Emergency_number { get; set; }
        /// <summary>
        /// 资格证书
        /// </summary>
        public string? Credentials { get; set; }
    }
}
