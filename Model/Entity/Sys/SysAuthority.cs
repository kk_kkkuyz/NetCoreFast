﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 权限
    /// </summary>
    [Serializable]
    [Table("Sys_Authority")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    [Index(nameof(Code))]
    public class SysAuthority : UserID
    {
        /// <summary>
        /// 权限名称，用于界面显示
        /// </summary>
        [Display(Name = "名称")]
        [Required(ErrorMessage = "名称必填")]
        public string Name { get; set; }
        /// <summary>
        /// 权限码，用于程序内部区别查找
        /// </summary>
        [Display(Name = "权限码")]
        [Required(ErrorMessage = "权限码必填")]
        [StringLength(50)]
        public string Code { get; set; }
        /// <summary>
        /// 权限对应的资源（一个权限有多重资源，一个资源可以被多个权限拥有）  一个资源对应一个权限？或者一个资源可以对应多个权限？哪种更优？  
        /// 。在做增加修改的的时候不需要给此属性赋值
        /// </summary>      
        //[Display(Name = "权限对应的资源")]
        //[SwaggerSchema(ReadOnly = true)]
        //[DtoGen(NotAddUpdate  = true)]
        //public virtual ICollection<AuthorityRes> Reses { get; set; } = new HashSet<AuthorityRes>();
        //private AuthorityGroup group;
        /// <summary>
        /// 简介
        /// </summary>
        [Display(Name = "简介")]
        public string? Intro { get; set; }
    }
}