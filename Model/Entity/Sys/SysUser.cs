﻿using Common.MyAttribute;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 用户
    /// </summary>
    [Serializable]
    [Table("Sys_User")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort))]
    [Index(nameof(Username), nameof(Tel), nameof(Email))]
    public class SysUser : Person
    {
        /// <summary>
        /// 部门id
        /// </summary>
        public int? DepartmentId { get; set; }
        /// <summary>
        /// 部门对象。客户端做增加修改时不需要给此属性赋值（修改对应的DepartmentId即可）
        /// </summary>
        [ForeignKey("DepartmentId")]
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public virtual SysDepartment Department { get; set; }//所在部
        /// <summary>
        /// 部门名称
        /// </summary>
        [NotMapped]
        [FromModel("Department.Name")]
        [DtoGen(NotAddUpdate = true)]
        public string DepartmentName { get; set; }
        public string? Wx_openid { get; set; }
        public string? Wx_unionid { get; set; }
    }
}
