﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 地区
    /// </summary>
    [Serializable]
    [Table("Sys_Area")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(ParentId))]
    [Index(nameof(Name))]
    public class SysArea : TreeID<SysArea>
    {
        public SysArea()
        {
            Childs = new HashSet<SysArea>();
        }
        /// <summary>
        /// 区域名称
        /// </summary>
        [Required(ErrorMessage = "区域名称必填")]
        [StringLength(50)]
        public string Name { get; set; }//名称     
    }
}
