﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Model.Entity.Sys.Enum
{
    /// <summary>
    /// 性别枚举，值： 1男, 2女
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// 男
        /// </summary>
        [Display(Name = "男")]
        man = 1,
        /// <summary>
        /// 女
        /// </summary>
        [Display(Name = "女")]
        woman = 2
    }
}
