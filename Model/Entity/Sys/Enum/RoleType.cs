﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;

namespace Model.Entity.Sys.Enum
{
    /// <summary>
    /// 角色类型,或身份
    /// </summary>
    public enum RoleType
    {
        /// <summary>
        /// 超级管理员
        /// </summary>
        admin = 0,
        /// <summary>
        /// 普通会员
        /// </summary>
        [Display(Name = "普通会员")]
        user = 1,
        [Display(Name = "教师")]
        teacher = 2,
        [Display(Name = "学生")]
        student = 3
    }

}