﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 文档
    /// </summary>
    [Serializable]
    [Table("Sys_Document")]
    [Index( nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    [Index(nameof(Name))]
    public class SysDocument : UserID
    {
        /// <summary>
        /// 文件在数据库中的显现名称.为空则显示为名字。
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 文件保存在磁盘的真实名称，目前不可更改
        /// </summary>
        [StringLength(127)]
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        [Required(ErrorMessage = "路径必填")]
        public string Path { get; set; }
        /// <summary>
        /// SIEZ为oracle的关键词 
        /// </summary>
        public float? Size { get; set; }
    }
}
