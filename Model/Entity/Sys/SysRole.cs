﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 角色
    /// </summary>
    [Serializable]
    [Table("Sys_Role")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    [Index(nameof(Name))]
    public class SysRole : UserID
    {
        /// <summary>
        /// 角色名称，用于界面显示
        /// </summary>
        [Display(Name = "名称")]
        [Required(ErrorMessage = "名称必填")]
        [StringLength(50)]
        public string Name { get; set; }
        /// <summary>
        ///   角色码，用于程序内部区别查找
        /// </summary>
        [Display(Name = "角色码")]
        [Required(ErrorMessage = "角色码必填")]
        public string Code { get; set; }
        /// <summary>
        /// 角色拥有的权限，一个角色可以拥有多个权限，一个权限可以属于多个角色，多对多关系。在做增加修改的的时候不需要给此属性赋值
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        // [DtoGen(NotAddUpdate = true)]
        // public virtual ICollection<RoleAuthority> Authorities { get; set; } = new HashSet<RoleAuthority>();
        /// <summary>
        /// 简介
        /// </summary>
        [Display(Name = "简介")]
        public string? Intro { get; set; }
    }
}