﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Model.Entity.Sys;
using Common.MyAttribute;

namespace Model.Entity
{
    /// <summary>
    /// 统一
    /// </summary>
    public abstract class UserTreeID<E> : TreeID<E>, IUser
    {
        /// <summary>
        /// 添加此记录的用户id，系统自动赋值
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(AddUpdateButInvisible =  true)]
        public int? UserId { get; set; }
        /// <summary>
        ///  添加此记录的用户对象，客户端做增加修改时不需要给此属性赋值（只需要传递修改UserId属性值）
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true,NotList =true)]
        [ForeignKey("UserId")]
        public virtual SysUser User { get; set; }
        /// <summary>
        /// 添加用户
        /// </summary>
        [NotMapped]
        [FromModel("User.Username")]
        [DtoGen(NotAddUpdate = true)]
        public string UserName { get; set; }
    }
}
