# 查询过滤器
查询过滤器Web.Filter.QueryFilterAttribute主要用于给**添加修改、查询删除**操作方法设置条件。   

## 1、查询删除过滤器  
### (1)服务端实现步骤
#### 第1步、给控制器或action方法添加[QueryFilter]属性注解
#### 第2步、操作方法命名包含["list", "delete", "batchdelete", "get", "query"]任何一个（不区分大小写）或者方法添加[Queryable]注解
#### 第3步、操作方法添加[FromQuery] Dictionary<string, string> 类型参数，用于接收客户端查询参数及QueryFilterAttribute过滤器设置预定义查询参数
#### 第4步、调用业务类具有Dictionary<string, string>参数类型的查询方法查询数据
```
 [Route("api/[controller]/[action]")]
 [ApiController]
 [QueryFilter]
public class AreaController: MyBaseController<Area>
  [HttpGet]
  public Result List([FromQuery] Dictionary<string, string> where)
  {
     return Result.Success("succeed").SetData(bll.Query(where));
  }
}
```
> QueryFilterAttribute内置预设查询参数有：         
> 1、对于查询方法"list", "query"：     
> a、如果用户登录信息包含tenantId，则表示多租户，则条件查询where增加TenantId查询租户的数据。即所属租户身份过滤，只能查询所属租户的数据（默认）       
> b、如果用户已经登录，且登录信息角色不以"admin"开头，则表示普通用户，查询条件增加UserId查询用户自己添加的数据。即非管理员只能查看自己的数据，管理员查看所有信息（默认）        
> c、如果客户端没传递排序参数sort，则设置sort为-Added_time，表示默认按照添加时间降序排列      
> 1、对于删除方法"delete"|"batchdelete"和查询单个get方法：    
> a、如果用户登录信息包含tenantId，则表示多租户，则条件查询where增加TenantId查询租户的数据。即所属租户身份过滤，只能查询（删除）所属租户的数据（默认）       
> b、如果用户已经登录，且登录信息角色不以"admin"开头，则表示普通用户，则条件查询where增加UserId查询（删除）用户自己添加的数据。即非管理员只能查看（删除）自己的数据，管理员查看（删除）所有信息（默认）        
> c、如果用户已经登录，且登录信息角色不以"admin"开头，则表示普通用户，则条件查询where添加Sys为false，表示非管理员不能删除（单个查询）系统数据。Sys为true表示系统数据 。  即非管理员（普通会员）不能删除系统数据（默认），非管理员（普通会员）不能获取系统数据（默认）    
> 
### (2)客户端查询写法   
#### a、list查询，查询列表。   
```
/api/area/list?sort=-Id&name=武汉&pageNo=1   
```
> sort，排序，值的格式为：+|-字段名,+|-字段名。如-Id根据Id字段递减排序；+Id根据Id字段递增排序。可多个字段排序：如+Id,-Added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致    
> pageNo，查询页码；<=0则查询所有，默认1。   
> pageSize，每页显示数量；<=0则查询所有，默认12。   
> 条件查询，格式：字段名__操作符=值。如"parent.id__eq=1"、 ParentId=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致    
> 条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,gte大于等于,lt小于, lte小于等于,in包含,between在……之间。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”    

#### b、get查询，查询单个   
```
/api/area/get/1   
```
#### c、id单个删除   
```
/api/area/delete/1
或
/api/area/delete?id=1
```
#### d、id批量删除   
```
/api/area/delete/1,2,3
或
/api/area/delete?id=1,2,3
```
#### e、条件批量删除  
```
/api/area/delete?name=武汉
```
#### 
## 2、增加修改过滤器     
### (1)服务端实现步骤   
#### 第1步、给控制器或action方法添加[QueryFilter]属性注解   
#### 第2步、操作方法命名包含"add", "update"任何一个（不区分大小写）或者方法添加[Queryable]注解   
#### 第3步、操作方法添加实体类型参数   
```
 [Route("api/[controller]/[action]")]
 [ApiController]
 [QueryFilter]
public class AreaController: MyBaseController<Area>
   [HttpPost]
   public Result Add(Area o)
   {
      return ModelState.IsValid ? (bll.Add(o) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
   }
}
```
> QueryFilterAttribute内置预设查询参数有：         
> 1、如果方法参数实体实现了IUser接口，并且用户登录，则给实体设置UserId值，给数据设置一个标志，表示登录用户进行了增加或修改   
```
//自动添加用户id
if (typeof(IUser).IsAssignableFrom(parameterType))
{
    var model = context.ActionArguments[parameterName] as IUser;
    if (userId != 0)
    {
        model.UserId = userId;
    }
}
```
> 2、如果方法参数实体实现了ITenant接口，并且用户已登录且租户TenantId存在，则给实体设置TenantId值，给数据设置一个标志，表示所属租户进行了增加或修改   
```
//自动添加租户id
if (typeof(ITenant).IsAssignableFrom(parameterType))
{
    var model = context.ActionArguments[parameterName] as ITenant;
    if (TenantId != 0)
    {
        model.TenantId = TenantId;
    }
}
```
> 3、如果方法参数实体实现了ISys接口，则自动给参数实体设置系统数据        
> a、自动设置Modify_time修改时间等于当前时间   
> b、如果登录用户角色以“admin”开头，表示管理员，则将实体的Sys设置为true，表示系统资源；如果登录用户角色不以“admin”开头，表示非管理员，则将实体的Sys设置为false，表示非系统资源；   
``` 
//如果是ISys类型，添加系统资源标志
if (typeof(ISys).IsAssignableFrom(parameterType))
{
    var model = context.ActionArguments[parameterName] as ISys;
    model.Modify_time = DateTime.Now;
    //添加系统资源标志  非系统管理员或者非校区管理员、学生、老师添加的资源为非系统资源。系统管理员、校区管理员添加的资源为系统资源                   
    if (role.StartsWith("admin"))
    {
        model.Sys = true;                          
    }
    else
    {
        model.Sys = false;
    }
}
```