﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
namespace CodeGen
{
    public class DownloadProject
    {
        public bool HttpDownFile()
        {
            string url = "http://localhost:99/api/HttpFile/GetFile";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            byte[] fileBytes;
            using (WebResponse webRes = request.GetResponse())
            {
                int length = (int)webRes.ContentLength;
                HttpWebResponse response = webRes as HttpWebResponse;
                Stream stream = response.GetResponseStream();
                var contentdisposition = response.Headers["Content-Disposition"];
                //var filename = Util.Midstr(contentdisposition, "filename=", ";");
                var filename = "ggg";
                //读取到内存
                MemoryStream stmMemory = new MemoryStream();
                byte[] buffer = new byte[length];
                int i;
                //将字节逐个放入到Byte中
                while ((i = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    stmMemory.Write(buffer, 0, i);
                }
                fileBytes = stmMemory.ToArray();//文件流Byte
                FileStream fs = new FileStream(@"D:\other\Test\"+filename, FileMode.OpenOrCreate);
                stmMemory.WriteTo(fs);
                stmMemory.Close();
                fs.Close();
                return true;
            }
        }
        /// <summary>
        /// 下载服务器文件并保存到客户端
        /// </summary>
        /// <param name="uri">被下载的文件地址，如：文件路径、url地址、ftp地址（包含文件名）</param>
        /// <param name="savePath">存放的目录（不包含文件名）</param>
        public static bool Download(string uri, string savePath)
        {
            //从文件路径中获取文件名
            string fileName;
            if (uri.IndexOf(@"\") > -1)
            {
                fileName = uri.Substring(uri.LastIndexOf(@"\") + 1);
            }
            else
            {
                fileName = uri.Substring(uri.LastIndexOf("/") + 1);
            }

            //设置文件保存路径：路径+""+文件名.后缀、路径+"/"+文件名.后缀
            if (!savePath.EndsWith("/") && !savePath.EndsWith(@"\"))
            {
                savePath = savePath + "/"; //也可以是savePath + "\"
            }

            savePath += fileName;   //另存为的绝对路径＋文件名

            //下载文件
            WebClient client = new WebClient();
            try
            {
                client.DownloadFile(uri, savePath);
            }
            catch (Exception ex)
            {
                // Logger.Error(typeof(DownloadFile), "下载文件失败", ex);
                return false;
            }

            return true;
        }
    }
}
