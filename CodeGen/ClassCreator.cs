﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Common.MyAttribute;
using Microsoft.CSharp;
using System.Diagnostics;

namespace CodeGen
{
    /// <summary>
    /// <see cref="https://blog.csdn.net/iml6yu/article/details/119858045"/>
    /// <see cref="https://blog.csdn.net/iml6yu/article/details/119858749"/>
    /// <see cref="https://docs.microsoft.com/zh-cn/dotnet/framework/reflection-and-codedom/how-to-create-a-class-using-codedom"/>
    /// <see cref="https://blog.csdn.net/C930363716/article/details/109475296"/>
    /// </summary>
    public class ClassCreator
    {
        /// <summary>
        /// Define the compile unit to use for code generation.
        /// </summary>
        private CodeCompileUnit? targetUnit;

        /// <summary>
        /// The only class in the compile unit. This class contains 2 fields,
        /// 3 properties, a constructor, an entry point, and 1 simple method.
        /// </summary>
        private CodeTypeDeclaration? targetClass;

        private List<string> namespaceNames;
        /// <summary>
        /// 字段
        /// </summary>
        private List<CodeMemberField> codeMemberFields;
        /// <summary>
        /// 属性
        /// </summary>
        private List<CodeMemberProperty> codeMemberPropertys;
        /// <summary>
        /// 父类（）接口集合
        /// </summary>
        private List<CodeTypeReference> baseTypes = new List<CodeTypeReference>();
        /// <summary>
        /// Define the class.
        /// </summary>
        public ClassCreator()
        {
            namespaceNames = new List<string>();
            codeMemberFields = new List<CodeMemberField>();
            codeMemberPropertys = new List<CodeMemberProperty>();
        }
        /// <summary>
        /// 添加父类 接口
        /// </summary>
        /// <param name="type"></param>
        public void AddBaseType(Type basetype)
        {
            baseTypes.Add(new CodeTypeReference(basetype));
        }
        /// <summary>
        /// 添加字段
        /// </summary>
        /// <param name="fileName">字段名称</param>
        /// <param name="type">字段类型</param>
        /// <param name="visitLevel">访问级别 默认是private</param>
        /// <param name="comments">字段描述信息</param>
        public void AddFields(string fileName, Type type, MemberAttributes visitLevel = MemberAttributes.Private, string comments = null, PropertyInfo p = null)
        {
            if (codeMemberFields.Exists(t => t.Name == fileName)) return;
            CodeMemberField field = new CodeMemberField();
            field.Attributes = visitLevel;
            field.Name = fileName;
            field.Type = new CodeTypeReference(type);
            if (p.PropertyType == typeof(DateTime?) || p.PropertyType == typeof(DateTime))
            {
                //object value = p.GetValue(Activator.CreateInstance(type));
                // Console.WriteLine(p.Name);                
                field.InitExpression = new CodeFieldReferenceExpression(new CodeTypeReferenceExpression("System.DateTime"), "Now");
            }
            field.Comments.Add(new CodeCommentStatement(
               " <summary>", true));
            field.Comments.Add(new CodeCommentStatement(
              comments ?? fileName, true));
            field.Comments.Add(new CodeCommentStatement(
              " </summary>", true));
            codeMemberFields.Add(field);
            AddNamespance(type);
        }


        /// <summary>
        /// 添加属性
        /// </summary>
        /// <param name="propertyName">属性名称</param>
        /// <param name="type">属性类型</param>
        /// <param name="comments">属性描述信息</param>
        public void AddProperties(string propertyName, Type type, bool hasGet = true, bool hasSet = true, string comments = null, IEnumerable<Attribute> customAttributes = null, PropertyInfo p = null)
        {
            if (codeMemberPropertys.Exists(t => t.Name == propertyName)) return;
            CodeMemberProperty property = new CodeMemberProperty();
            property.Attributes = MemberAttributes.Public;
            property.Name = propertyName;
            property.HasGet = hasGet;
            property.HasSet = hasSet;
            property.Type = new CodeTypeReference(type);
            property.Comments.Add(new CodeCommentStatement(
             " <summary>", true));
            property.Comments.Add(new CodeCommentStatement(
              comments ?? propertyName, true));
            property.Comments.Add(new CodeCommentStatement(
              " </summary>", true));
            var fileName = (propertyName[0]).ToString().ToLower() + propertyName.Substring(1);
            if (!codeMemberFields.Exists(t => t.Name == fileName))
                AddFields(fileName, type, MemberAttributes.Private, comments, p);
            //添加特性注解
            if (customAttributes != null)
            {
                foreach (var c in customAttributes)
                {
                    if (c.GetType() == typeof(RequiredAttribute))
                    {
                        RequiredAttribute ca = (RequiredAttribute)c;
                        CodeAttributeArgument codeAttr = new CodeAttributeArgument("ErrorMessage", new CodePrimitiveExpression(ca.ErrorMessage));
                        var codeAttrDecl = new CodeAttributeDeclaration(new CodeTypeReference(c.GetType()), codeAttr);
                        property.CustomAttributes.Add(codeAttrDecl);
                    }
                    if (c.GetType() == typeof(DisplayAttribute))
                    {
                        DisplayAttribute ca = (DisplayAttribute)c;
                        CodeAttributeArgument codeAttr = new CodeAttributeArgument("Name", new CodePrimitiveExpression(ca.Name));
                        var codeAttrDecl = new CodeAttributeDeclaration(new CodeTypeReference(c.GetType()), codeAttr);
                        property.CustomAttributes.Add(codeAttrDecl);
                    }
                    if (c.GetType() == typeof(StringLengthAttribute))
                    {
                        StringLengthAttribute ca = (StringLengthAttribute)c;
                        CodeAttributeArgument codeAttr = new CodeAttributeArgument("ErrorMessage", new CodePrimitiveExpression(ca.ErrorMessage));
                        CodeAttributeArgument codeAttr1 = new CodeAttributeArgument(new CodePrimitiveExpression(ca.MaximumLength));
                        CodeAttributeArgument codeAttr2 = new CodeAttributeArgument("MinimumLength", new CodePrimitiveExpression(ca.MinimumLength));
                        var codeAttrDecl = new CodeAttributeDeclaration(new CodeTypeReference(c.GetType()), codeAttr1, codeAttr2, codeAttr);
                        property.CustomAttributes.Add(codeAttrDecl);
                    }
                    if (c.GetType() == typeof(SwaggerSchemaAttribute))
                    {
                        SwaggerSchemaAttribute ca = (SwaggerSchemaAttribute)c;
                        CodeAttributeArgument codeAttr = new CodeAttributeArgument("ReadOnly", new CodePrimitiveExpression(true));
                        var codeAttrDecl = new CodeAttributeDeclaration(new CodeTypeReference(c.GetType()), codeAttr);
                        property.CustomAttributes.Add(codeAttrDecl);
                    }
                    if (c.GetType() == typeof(FromModelAttribute))
                    {
                        FromModelAttribute ca = (FromModelAttribute)c;
                        CodeAttributeArgument codeAttr = new CodeAttributeArgument(new CodePrimitiveExpression(ca.ModelColumnPath));
                        //CodeAttributeArgument codeAttr = new CodeAttributeArgument(new CodePrimitiveExpression(ca.ModelColumnPath));
                        var codeAttrDecl = new CodeAttributeDeclaration(new CodeTypeReference(c.GetType()), codeAttr);
                        property.CustomAttributes.Add(codeAttrDecl);
                    }
                    //CodeAttributeArgument codeAttr=new CodeAttributeArgument(new CodePrimitiveExpression("This class is obsolete."));
                    //var codeAttrDecl = new CodeAttributeDeclaration(new CodeTypeReference(c.GetType()), codeAttr);
                    // var  configParamAttr = new CodeAttributeDeclaration { Name = "MyAttribute",  new CodeAttributeArgument[] { new CodeAttributeArgument { Name = "ResourceType", Value = new CodePrimitiveExpression("typeof(Resources)") } } } ;

                    //property.CustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference(c.GetType())));
                }
            }
            property.GetStatements.Add(new CodeMethodReturnStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fileName)));
            CodeFieldReferenceExpression reference = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fileName);
            property.SetStatements.Add(new CodeAssignStatement(reference, new CodeArgumentReferenceExpression("value")));
            codeMemberPropertys.Add(property);
            AddNamespance(type);
        }

        private void AddNamespance(Type type)
        {
            if (namespaceNames.IndexOf(type.Namespace) == -1)
                namespaceNames.Add(type.Namespace);
        }

        /// <summary>
        /// 生成代码
        /// </summary>
        /// <param name="namespaceName">命名空间</param>
        /// <param name="className">类型</param>
        /// <param name="visitLevel">访问级别，默认是Public</param>
        /// <param name="filePath">文件完整路径，默认是程序所在目录，和类名同名</param>
        /// <param name="typeComments">类注释</param>
        /// <returns>true | false</returns>
        public bool GenerateCode(string namespaceName, string className, TypeAttributes visitLevel = TypeAttributes.Public, string? filePath = null, string typeComments = null)
        {
            var ret = GenerateCode(namespaceName, className, visitLevel, typeComments);
            if (string.IsNullOrEmpty(ret)) return false;
            using (StreamWriter sourceWriter = new StreamWriter(filePath ?? $"{className}.cs"))
            {
                sourceWriter.Write(ret);
                return true;
            }
        }
        /// <summary>
        /// 生成代码
        /// </summary>
        /// <param name="namespaceName">命名空间</param>
        /// <param name="className">类型</param>
        /// <param name="visitLevel">访问级别，默认是Public</param>
        /// <param name="filePath">文件完整路径，默认是程序所在目录，和类名同名</param>
        /// <param name="typeComments">类注释</param>
        /// <returns>String</returns>
        public String GenerateCode(string namespaceName, string className, TypeAttributes visitLevel = TypeAttributes.Public, string typeComments = null)
        {
            try
            {
                targetUnit = new CodeCompileUnit();
                CodeNamespace newClass = new CodeNamespace(namespaceName);
                targetClass = new CodeTypeDeclaration(className);
                targetClass.IsClass = true;
                targetClass.TypeAttributes = visitLevel;
                targetClass.IsPartial = true;
                targetClass.BaseTypes.AddRange(baseTypes.ToArray());//添加父类
                targetClass.Comments.Add(new CodeCommentStatement(
             " <summary>", true));
                targetClass.Comments.Add(new CodeCommentStatement(
                  typeComments ?? "自动生成类", true));
                targetClass.Comments.Add(new CodeCommentStatement(
                  " </summary>", true));

                newClass.Types.Add(targetClass);
                targetUnit.Namespaces.Add(newClass);

                namespaceNames.ForEach(item =>
                {
                    newClass.Imports.Add(new CodeNamespaceImport(item));
                });

                codeMemberFields.ForEach(item =>
                {
                    targetClass.Members.Add(item);
                });

                codeMemberPropertys.ForEach(item =>
                {
                    targetClass.Members.Add(item);
                });

                CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
                CodeGeneratorOptions options = new CodeGeneratorOptions();
                options.BracingStyle = "C";
                using (StringWriter sourceWriter = new StringWriter())
                {
                    provider.GenerateCodeFromCompileUnit(
                        targetUnit, sourceWriter, options);
                    return sourceWriter.ToString();
                }

            }
            catch (Exception)
            {
                return "";
            }

        }
        /// <summary>
        /// 加载.cs类
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<Type> LoadTypes(string path)
        {
            var ts=  new List<Type>();
           var fs= GetFiles(path);
            fs.ForEach(f =>
            {
                Console.WriteLine(File.ReadAllText(f, Encoding.UTF8));
                Compiler.Compile(File.ReadAllText(f, Encoding.UTF8), Assembly.Load(new AssemblyName("System.Runtime")), typeof(object).Assembly);
            });
          
            return new List<Type>();
        }
       
        /// <summary>
        /// 获取路径下的cs文件
        /// </summary>
        /// <param name="path">目录路径</param>
        /// <returns></returns>
        public static List<string> GetFiles(string path)
        {
            List<string> fileList = new List<string>();
            if (Directory.Exists(path) == true)
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    if (file.EndsWith(".cs"))
                    {          
                        fileList.Add(file);
                    }
                }
                foreach (string directory in Directory.GetDirectories(path))
                {
                    fileList.AddRange(GetFiles(directory));
                }
            }
            return fileList;
        }

       
    }
}
