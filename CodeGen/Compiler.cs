﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodeGen
{
    /// <summary>
    /// <seealso cref="https://www.cnblogs.com/fanfan-90/p/13902763.html"/>
    /// </summary>
    public class Compiler
    {
        /// <summary>
        /// Compile方法将会对参数sourceCode提供的源代码进行编译。该方法返回源代码动态编译生成的程序集，它的第二个参数代表引用的程序集。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="referencedAssemblies"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static Assembly Compile(string text, params Assembly[] referencedAssemblies)
        {
            var references = referencedAssemblies.Select(it => MetadataReference.CreateFromFile(it.Location));
            var options = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary);
            var assemblyName = "_" + Guid.NewGuid().ToString("D");
            var syntaxTrees = new SyntaxTree[] { CSharpSyntaxTree.ParseText(text) };
            var compilation = CSharpCompilation.Create(assemblyName, syntaxTrees, references, options);
            using (var stream = new MemoryStream())
            {
                var compilationResult = compilation.Emit(stream);
                if (compilationResult.Success)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    return Assembly.Load(stream.ToArray());
                }
                throw new InvalidOperationException("Compilation error");
            }
        }
    }
}
